'''
Created on Mar 5, 2014

@author: merca
'''
from AParagraphParser import AParagraphParser
from Constants import *
from OutputMofEstablish import OutputMofEstablish
import re
from symbol import try_stmt
from compiler.pycodegen import TRY_FINALLY

class ParagraphParserEstablish(AParagraphParser):
    '''
    Implementation of the abstract parser, that checks if a paragraph is of 
    the type Establish and extract data from it
    '''


    def __init__(self, paragraphText):
        AParagraphParser.__init__(self, paragraphText)
        self.output = OutputMofEstablish();
        self.numberInfo = self.countNumberInfo()
        
    def getType(self):
        ''' find the type of the paragraph '''
        if self.testEstablish() == True:
            return ParagraphType.Constitutiv
        else:
            return ParagraphType.OTHER
    
    
    def testEstablish(self):
        '''
        test if the given paragraph defines a establishment of a company 
        check if the paragraph contains a number of keywords
        '''
        
        splitted = self.paragraphText.split('\n')
        ok = 0
        size = len(establishKeyWords)
        for keywrd in establishKeyWords:
            establishKeyWords[keywrd] = 0
        
        count = 0
        
        ''' iterate line by line checking if the line contains 
            one of the keywords, count the occurrences  '''
        for line in splitted:
            for keyword in  establishKeyWords:
                if keyword in line:
                    establishKeyWords[keyword] += 1
                
                count = self.checkCount(establishKeyWords)
                if (count >= size - 4):
                    ok = 1
                    break
            if ok == 1:
                break
        
        #print "Count-ul este " + str(count) + " Size este " + str(size)
        
        if ok == 1:
            return True
        else:
            return False

    @staticmethod
    def checkCount(words):
        '''
            count the number of non zero values from a dictionary
        '''
        count = 0
        
        for y in words.items():
            if y[1] != 0:
                count += 1
        return count 
    
    
    
    def parseText(self):
        
        splitted = self.paragraphText.split('\n')
        
        currentInfo = 0
        output = OutputMofEstablish()
        output.denumireSocietate = splitted[1]
        
        start = 0
        
        regexinfo = re.compile(regexInfoEstablish)
        index = 0
        
        while (index < len(splitted)):
            line = splitted[index]
            
            if start == 0:
                for keyw in establishStart:
                    if keyw in line:
                        establishStart[keyw] += 1
            
            
            if self.checkCount(establishStart) > 2:
                #print "AICISEA+++++++++++++++"
                start = 1
                matched = regexinfo.match(line)
                info = ""
                
                if matched != None:
                    currentInfo += 1
                    index, info = self.getUntil(splitted, index, currentInfo)   
                    #print "Info este " + info
                    #print "Current line " + splitted[index]
                    self.__getInfo(info)
            
            index += 1
    
    
    def getUntil(self, splitted, index, numberinfo):
        
        regexend = re.compile(".*[;.]$")
        regexnumb = re.compile("^([1-9].\s).*")
        regexinfo = re.compile(regexInfoEstablish)
        
        info = ""
        linenumber = 0
        ok = 0
        ok1 = 0
        
        line = splitted[index]
        
        normalend = 0
        nextend = 0
        
        """ while is not the end of a line or the next line is a new 
            information """
        while True:
            #print "The line is " + line
            
            """ if the information ends normally with ; """
            if (regexend.match(line.strip()) != None and
                    numberinfo == self.numberInfo):
                normalend = 1
                #print "Ies normal"
                break
            
            """ if the information section ends with the next line being a new
                information """
            if index+1 < len(splitted):
                if regexinfo.match(splitted[index+1]) != None:
                    nextend = 1
                    #print "Ies informatie noua next"
                    break
            
            """ if the current line is the second line in a information and it's
                a bulleted number (ex #.) then get all the lines until a new
                information """
            if linenumber == 1 and regexnumb.match(line.strip()) != None:
                #print "Intru cu line number 1"
                """ wait for the next line to be a new info line """
                while regexinfo.match(line.strip()) == None:
                    
                    info += line + " "
                    ok1 = 1
                    index += 1
                    if index >= len(splitted):
                        break
                    line = splitted[index] + "\n"
                ok = 1
            
            
            if ok == 1:

                index -= 1
                break
            
            info += line + " "
            
            index += 1
            if index >= len(splitted):
                break

            linenumber += 1
            line = splitted[index] + "\n"
        
        if ok1 != 1:
            info += line + " "
#         else:
#             index -= 1
        
            
        return index, info
            
        
    @staticmethod
    def after(line):
        index = line.find(":")
        if index != -1:
            return line[index+1:]
        else:
            return ""
        
    @staticmethod
    def before(line):
        index = line.find(":")
        if index != -1:
            return line[0:index]
        else:
            return ""
        
    
    def __getInfo(self, infoline):
        '''
            get a specific information from a info line set the output
        '''
        before_2p = ParagraphParserEstablish.before(infoline)
        after_2p = ParagraphParserEstablish.after(infoline)
        
        if any(key in before_2p for key in IDCOMPNAME):
            self.output.denumireSocietate = after_2p
        
        if any(key in before_2p for key in IDCOMPCUI):
            self.output.cuiSocietate = after_2p
        
        if any(key in before_2p for key in IDCOMPREG):
            self.output.nrOrdRegCom = after_2p
        
        if any(key in before_2p for key in IDCOMPHQ):
            self.output.sediuSocial = after_2p
        
        if any(key in before_2p for key in IDCOMPMAD):
            self.output.domeniuActivitate = after_2p
        
        if any(key in before_2p for key in IDCOMPMA):
            self.output.activitatePrincipala = after_2p
        
        if any(key in before_2p for key in IDCOMPCS):
            self.output.capital = after_2p
        
        if any(key in before_2p for key in IDCOMPFD):
            self.output.fondatori = after_2p
        
        if any(key in before_2p for key in IDCOMPAD):
            self.output.administratori = after_2p
        
        if any(key in before_2p for key in IDCOMPDF):
            self.output.durataFunc = after_2p
            
    
    def countNumberInfo(self):
        splitted = self.paragraphText.split('\n')
        regexinfo = re.compile(regexInfoEstablish)
        i = 0
        nr = 0
        while (i < len(splitted)):
            if regexinfo.match(splitted[i]) != None:
                nr += 1
            i += 1
        return nr
        