'''
Created on Mar 5, 2014

@author: merca
'''

from Constants import ParagraphType
from AOutputMof import AOutputMof


class AParagraphParser :
    '''
    This class is a abstract class that contains the functionality of a abstract
    parser, for each type of parser a this class will be inherited and the 
    functions parseText and getType will be implemented.
    '''
    

    def __init__(self, paragraphText = ""):
        '''
        @param paragraphText: this is the text to be parsed a paragraph of a 
                                mof
        @var self.type: the type of a paragraph a kind of enum
        @var self.output : the type of the output
        '''
        self.type = ParagraphType()
        self.output = AOutputMof()
        self.paragraphText = paragraphText
        
    def parseText(self):
        """
        this is a abstract method that will be implemented in a child class
        """
        
    def getType(self):
        """ 
        this is a abstract method that will be implemented in a child class
        """
        
    