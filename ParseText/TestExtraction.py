'''
Created on Apr 28, 2014

@author: merca
'''

import sys

import re
from ParagraphParserEstablish import ParagraphParserEstablish
from ParagraphParserEstablish import ParagraphType

class ExtractionTest():
    OUTPUTDIR = "/home/merca/businessGraph/mofDownText/Test"
    EXPECTEDOUT = "/home/merca/businessGraph/mofDownText/Test/expected"
    
    
    def testExtraction(self):
        with open(self.EXPECTEDOUT) as f:
            content = f.readlines()
        totalExpected = 0
        totalExtracted = 0
        for x in content:
            splitted = x.split("\t")
            expectedArticles = splitted[1].strip()
            expectedInput = splitted[0].strip()
            
            inputFile = self.OUTPUTDIR + "/" + expectedInput
            utile, total = self.parse(inputFile)
            if (int(utile) == int(expectedArticles)):
                print "Success " + str(expectedInput)
            else:
                print ("File: " + str(expectedInput) +  " Expected: " +
                str(expectedArticles) + " Extracted: " + str(utile))
            totalExpected += int(expectedArticles)
            totalExtracted += int(utile)
            
        print "Am extras: " + str(100.0 * totalExtracted / totalExpected ) + "%"
        
        
    def parse(self, fisier):
        
        with open(fisier) as f:
            content = f.readlines()
        
        iterator = iter(content)
        count1 = 0
        count2 = 0
         
        while True:
            try:
                line = iterator.next()
            except StopIteration:
                break
            
            regx = re.compile("^[(][0-9/.]*[)]$")
            toparse = ""
            ok = 0
            while regx.match(line) == None:
                toparse += line
                try:
                    line = iterator.next()
                except StopIteration:
                    ok = 1
                    break
            
            count2 += 1
            
            parse = ParagraphParserEstablish(toparse)
            
            if parse.getType() != ParagraphType.OTHER:
                
                
                parse.parseText()
                count1 += 1
                
            if ok == 1:
                break
        
        return count1, count2

if __name__ == '__main__':
    test = ExtractionTest()
    test.testExtraction()
    
    
    
    