# -*- coding: utf-8 -*-

from ParagraphParserEstablish import ParagraphParserEstablish
import re
from Constants import ParagraphType
import sys
import os
from xdg.Locale import regex
from __builtin__ import exit

totalutil = 0
totaltotal = 0

if len(sys.argv) < 3:
    print "Should be runned with python main.py input_directory output_db_name"
    exit()
     

for fisier in os.listdir(sys.argv[1]):
    print sys.argv[1] + fisier
    with open(sys.argv[1] + fisier) as f:
        content = f.readlines()
    
    iterator = iter(content)
    count1 = 0
    count2 = 0
     
    while True:
        try:
            line = iterator.next()
        except StopIteration:
            break
        
        regx = re.compile("^[(][0-9/.]*[)]$")
        toparse = ""
        ok = 0
        while regx.match(line) == None:
            toparse += line
            try:
                line = iterator.next()
            except StopIteration:
                ok = 1
                break
              
        count2 += 1
        
        regrpl = re.compile('–')
        toparse = regrpl.sub('-', toparse)
        parse = ParagraphParserEstablish(toparse);
        if parse.getType() != ParagraphType.OTHER:
            
            parse.parseText()
            
            count1 += 1
            parse.output.insertdb(sys.argv[2])
         
        if ok == 1:
            break
    
    totalutil += count1
    totaltotal += count2
     
    print "Utile : " + str(count1)
    print "Total: " + str(count2)

print "Total util " + str(totalutil)
print "TOTAL TOTAL" + str(totaltotal)


# toparse = """Societatea Comercială 
# RILOVAL AGUA FILIALA SLATINA - S.R.L.
# Slatina, județul Olt
# ROMÂNIA
# MINISTERUL JUSTIȚIEI
# OFICIUL NAȚIONAL AL REGISTRULUI COMERȚULUI
# OFICIUL REGISTRULUI COMERȚULUI 
# DE PE LÂNGĂ TRIBUNALUL OLT
# EXTRAS AL REZOLUȚIEI NR. 4727/28.10.2010
# Director 
# la Oficiul Registrului 
# Comerțului de pe lângă 
# Tribunalul Olt - Barbulescu Maria
# În baza cererii nr. 28827 din data de 26.10.2010 și a
# actelor doveditoare depuse, s-a dispus autorizarea
# constituirii și înregistrarea societății comerciale cu
# următoarele date:
# - fondatori: 
# 1. Juan Domingo Royo Perez (CNP A5274212400),
# asociat, aport la capital 20,00 RON, echivalând cu 2 părți
# sociale, reprezentând 10 % din capitalul social total, cota
# de participare la beneficii/pierderi de 10 %;
# 2. S.C. RILOVAL SL (CIF B-46216545), aport la
# capital 180,00 RON, echivalând cu 18 părți sociale,
# reprezentând 90 % din capitalul social total, cu sediul în
# Spania, localitatea Valencia (Spania), str. Masia Del
# Juez, nr. 147, 4690 Torrent; 
# - administratori persoane fizice: Vicente Espeleta
# Blasco (A5274239600), în calitate de administrator,
# domiciliat în Spania, localitatea Valencia, Str. C. Maria
# Auxiliadora 21B, TO, durata mandatului nelimitată, având
# puteri depline;
# - denumire: RILOVAL AGUA FILIALA SLATINA -
# S.R.L.;
# - sediul social: municipiul Slatina, str. George Poboran
# nr. 20, camera nr. 5;
# - domeniul principal de activitate: grupa CAEN 429 -
# lucrări de construcție a altor proiecte inginerești;
# - activitate principală: cod CAEN 4291 - construcții
# hidrotehnice;
# - capitalul social subscris: 200,00 RON, vărsat
# integral; capitalul social este divizat în 20 părți sociale a
# 10,0000 RON fiecare;
# - durata de funcționare: nelimitată;
# - cod unic de înregistrare: 27636043;
# - număr de ordine în registrul comerțului: 
# J 28/515/2010.
# Dispune înregistrarea în registrul comerțului a datelor
# din declarația/declarațiile tip pe proprie răspundere.
# Dispune publicarea în Monitorul Oficial al României,
# Partea a IV-a, a extrasului prezentei rezoluții. 
# Executorie de drept.
# Cu drept de plângere, în termen de 15 zile, la
# Tribunalul Olt, în condițiile art. 6 alin. (3) - (5) din O.U.G.
# nr. 116/2009.
# Pronunțată în ședința din data de 28.10.2010.
# (21/1.856.413)"""
#    
# parse = ParagraphParserEstablish(toparse);
# 
# if parse.getType() != ParagraphType.OTHER:
#     parse.parseText()
# print parse.output
# 
#   
 
 
 
     
