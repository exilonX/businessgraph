'''
Created on Mar 5, 2014

@author: Merca Ionel
'''

class AOutputMof:
    '''
    abstract output class that contains the properties of a generic output from
    a paragraph of a mof
    '''
    denumireSocietate = ""
    cuiSocietate = ""
    nrOrdRegCom = ""


    def __init__(self, denumireSoc = "", cuiSoc = "", nrOrd = ""):
        '''
        constructor
        @param denumireSoc: the name of the company
        @param cuiSoc: unique registration code
        @param nrOrd: number in the registry of commerce
        '''
        
        self.cuiSocietate = cuiSoc
        self.denumireSocietate = denumireSoc
        self.nrOrdRegCom = nrOrd
        
        
    def __str__(self):
        '''
        to string function for the output
        '''
        result = ""
        result += "Denumire Societate : " + self.denumireSocietate + "\n"
        result += "CUI Societate : " + self.cuiSocietate + "\n"
        result += ("Nr de ordine Registrul Comertului Societate : " + 
                    self.nrOrdRegCom + "\n")

        return result
    
    