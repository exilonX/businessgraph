'''
Created on Mar 5, 2014

@author: merca
'''
from AOutputMof import AOutputMof
from pymongo import MongoClient

class OutputMofEstablish(AOutputMof):
    '''
    classdocs
    '''

    def __init__(self):
        self.persoanaDesemnata = ""
        self.sediuSocial = ""
        self.domeniuActivitate = ""
        self.activitatePrincipala = ""
        self.capital = ""
        self.fondatori = ""
        self.administratori = ""
        self.durataFunc = ""
        
    
    def __str__(self):
        result = ""
        result =  AOutputMof.__str__(self)
#         result += "Denumire Societate : " + self.denumireSocietate + "\n"
#         result += "CUI Societate : " + self.cuiSocietate + "\n"
#         result += ("Nr de ordine Registrul Comertului Societate : " + 
#                     self.nrOrdRegCom + "\n")
        result += "Sediu Social: " + self.sediuSocial + "\n"
        result += "Domeniu Principal de activitate: " + self.domeniuActivitate + "\n"
        result += "Activitatea Principala: " + self.activitatePrincipala + "\n"
        result += "Capital Social: " + self.capital + "\n"
        result += "Fondatori: " + self.fondatori + "\n"
        result += "Administratori: " +  self.administratori + "\n"
        result += "Durata de Functionare: " + self.durataFunc + "\n"
        return result
    
    def checkSet(self, tocheck):
        '''
            check if the tocheck field is set
        '''
    
    def check_empty_fields(self):
        if (self.domeniuActivitate != "" or self.denumireSocietate != "" or
            self.activitatePrincipala != "" or self.cuiSocietate != "" or
            self.administratori != "" or self.fondatori != "" or
            self.durataFunc != "" or self.capital != "" or
            self.nrOrdRegCom != "" or self.sediuSocial != ""):
            return True
        return False
    
    def insertdb(self, dbname):
        client = MongoClient('localhost', 27017)
        db = client[dbname]
        col = db['infiintari']
        
        if self.check_empty_fields():
            
            output = {
                      "denumire" : self.denumireSocietate,
                      "cui" : self.cuiSocietate,
                      "nr_ordine" : self.nrOrdRegCom,
                      "sediu_social" : self.sediuSocial,
                      "dom_activ" : self.domeniuActivitate,
                      "activ_princ" : self.activitatePrincipala,
                      "capital" : self.capital,
                      "fondatori" : self.fondatori,
                      "admin" : self.administratori,
                      "durata_func" : self.durataFunc
                      }
            
            col.insert(output)
        
        
    
        
    