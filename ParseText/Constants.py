# -*- coding: utf-8 -*-

"""
    enum class - a certain type of paragraph
"""
class ParagraphType:
    Constitutiv, Asociere, Respingere, Dizolvare, OTHER = range(5)

"""
    the keywords that define a establishment of a company
"""
establishKeyWords = ( {'OFICIUL NAȚIONAL AL REGISTRULUI COMERȚULUI' : 0,
                       'constituirii' : 0,
                       'înmatricularea' : 0,
                       'înregistrarea' : 0,
                       'fondatori' : 0,
                       'fondator' : 0,
                       'administrator' : 0,
                       'denumire' : 0,
                       'capital' : 0
                        }
                     )

"""
    the first part of a establishment keywords
"""
establishStart = ({'constituirii' : 0,
                   'înmatricularea' : 0,
                   'înregistrarea' : 0,
                   'constituiri' : 0
                   })

"""
    regex to check if a part of the text defines information about a company
"""
regexInfoEstablish = "^[-–] .*:.*"

""" id of the name of a society """
IDCOMPNAME = {"denumire", "Denumire Societate"}

""" vector of posible id's for unique registration code """
IDCOMPCUI = ({"cod unic", "cod unic de înregistrare", 
              "CUI", "C.U.I", "CIF", "C.I.F"})

''' vector of possible id's for registration number '''
IDCOMPREG = ({"număr de ordine în registrul comerțului", 
              "nr. de ordine în registrul comerțului",
              "nr. de ordine"
              })

''' vector of possible id's for the address of a company '''
IDCOMPHQ = ({"sediul social",
             "sediul"
             })

''' vector of possible id's for the main activity domain of the company '''
IDCOMPMAD = ({ "domeniul principal de activitate",
              "domeniul de activitate",
              "grupa CAEN" 
              })

''' vector of possible id's for the main activity of the company '''
IDCOMPMA = ({"activitate principală", "activitatea principală"
             })

''' vector of id's for the social capital '''
IDCOMPCS = ({"capital social"})

''' vector of id's for the founders '''
IDCOMPFD = ({"fondatori", "fondator", "date de identificare fondator", "director",
              "datele de indentificare ale fondator"})

''' vector of id's for administrators '''
IDCOMPAD = ({"administrator", "administrator și reprezentant", "administratori"})

''' vector of id's for '''
IDCOMPDF = ({"durata de funcționare", "durata de functionare"})
