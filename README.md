# README #

This README would normally document whatever steps are necessary to get your application up and running. https://docs.google.com/file/d/0B-M0-nz3PrF-d3pGczdvSTlZdlE/edit

### What is this repository for? ###

## Quick summary ##

The project aims to create the graph of Romanian businessmen using different techniques of information extraction to harvest the names of companies and businessmen and determine the connections between them. This is a first attempt to build the graph for Romanian businessmen and companies. The resulted graph is analyzed in Gephi and the most important metrics regard connectivity and node ranking based on different metrics like betweenness, closeness, eccentricity and eigenvector centrality.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

# Get pdf #

Create mofDown directory in the root directory of the project.
Execute get_pdf.py script. Ex. *"python get_pdf.py 2001"* this will download all the available documents in mofDown/2001

# Extract text from pdf to text files #

Create directory mofDownText in the root directory
Test extraction is done using extract.jar which accepts as a argument the location of the directory that contains the pdf files.
Ex: *"java -jar extract.jar mofDown/2001 text"* this will create a directory 2001 in mofDownText and will put the text files there. 

# Extract text and index to Solr #

In order to start indexing you should first download solr from here: https://lucene.apache.org/solr/downloads.html
Then start the server : https://lucene.apache.org/solr/4_9_0/tutorial.html
Ex: *"java -jar extract.jar mofDown/2001 index"* this will add a index into SOLR with the paragraphs from the year 2001.

# Extract and add to mongodb #

Information can be extracted from text files using the python Parser (ParseText python project).
In order to extract the information from a specfic year and add it to a NoSQL database MongoDB you first have to download mongodb and run the 
mongo server. Verify that the mongo server is listening on port 27017.
After you have started the server you can run the extraction program "python main.py input_directory output_db_name" the input_directory should
contain Official Gazettes (MOF) in a txt format.
The script will parse each paragraph from each file and if it's an establishment paragraph it will extract the required information and add new key-value
pairs into the database that you specified output_db_name.
You can download an existing mongodb dump database from here [dump download](https://drive.google.com/folderview?id=0B05iCiq9eeevWXZwR3ZEUVl5Q2c&usp=sharing)
This database contains the information extracted from 2014, 2001-2011.

# Export GEXF # 

The GEXF format is useful to load the graph into GEPHI. The graph can be exported after it's built using data from the mongodb database described previously.
The project GraphService contains this feature, in the Main class you must call the function mongodbdata, this function creates the graph (CompGraph object for 
the company graph and Graph for the businessmen graph so you must select in which of these two graphs you want to add nodes to, in order to do this you must comment
the desired line (graf.addNode(info); or compGraf.addNode(info);).
The export process can be done like this:
		GraphModel grafModel = Export.makeGephiGraph(graf);
		Export.exportGEXF("CompGraf.gexf", grafModel);   // the first argument is the name of the output file
This must be done obviously after the graph was built.

# Export to Neo4j # 

In order to export the graph into Neo4j you don't have to do something special because the required jar file is already added to the project.
Similar to exporting to GEXF you must build the graph and instead of exporting to Gexf you must call the function:
                 exportNeoComp(graf); // the parameter is the CompanyGraph or the Graph (Business Men graph)


# Run the server # 

In order to run the TomcatServer you must download eclipse for jee developers and install Tomcat.
The class TestServlet must be run on a TomcatServer this accepts HTTP GET requests with the name of the businessMan 
"http://localhost:8080/GraphService/TestServlet?id=Traian Basescu" and returns a maximum of five connected components 
in which this name has a node match.
The response is a JSON file with the connected components and information about that component.
This feature can be tested using the proxy from the folder exilonx.github.io. This requires a webserver.
Let's assume you're using WAMP or XAMPP you must create a folder graph and copy the contents of exilonx.github.io there.
After doing this you can run the server and access http://localhost/graf/exilonX.github.io/proxy.php?id=ION this will make a 
request to query the graph for ION. 
The web interface can be tested preferably in Firefox and it can be accessed http://localhost/graf/exilonX.github.io/ this requires
the TestServlet class to be runned and the Neo4j database location be set here: 
                 public static String DB_PATH = "D:\\workspace-jee\\GraphService\\graphneo.db3";





### Who do I talk to? ###

* Merca Ionel ionel.merca@gmail.com