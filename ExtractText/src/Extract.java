
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.TextNormalize;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;


public class Extract {

	  private static final long MEGABYTE = 1024L * 1024L;

	  public static long bytesToMegabytes(long bytes) {
	    return bytes / MEGABYTE;
	  }

	
	/**
	 * @param args
	 * args[0] the path to the input directory mofDown/year/
	 * args[1] where to put the extracted text:
	 * 		-	index - indexed in solr
	 * 		-	text  - text file
	 */
	public static void main(String[] args) {
		File pdf_path = new File(args[0]);
		
		if (pdf_path.exists()) {
			// Daca calea exista atunci listeaza toate fisierele din interiorul 
			// directorului
			ArrayList<File> pdfs = new ArrayList<>(Arrays.asList(pdf_path.listFiles()));
			System.out.println("Am de procesat " + pdfs.size());
			int index = 0;
			HttpSolrServer server = null;
			
			if(args[1].equals("index")) {
				server = new HttpSolrServer("http://localhost:8983/solr");
			}
			
			for (int i = 0; i < pdfs.size(); i++) {
				index = 0;
				System.out.println("Indexul este : " + i);
				
				// luam fisierul input pdf si formam calea fisierului de output text
				File input = pdfs.get(i);
				String input_name = input.getPath();
				
				int first_slash = input_name.indexOf('/');
				String outtext = "mofDownText" + input_name.substring(first_slash);
				
				int last_slash = outtext.lastIndexOf('/');
				String folder_text = outtext.substring(0, last_slash);
				File directory = new File(folder_text);
				
				// verifica daca directorul a fost creat daca nu 
				// se creaza directorul si de vor adauga fisierele text in el
				if (!directory.exists()) {
					System.out.println("creating directory: " + folder_text);
					boolean result = directory.mkdir();  

					if(result) {
						System.out.println("DIR created");  
					}
				}
				
				PDDocument pd;
				
				// numele fisierului txt de iesire
				String textname = outtext.replaceFirst("pdf", "txt");
				System.out.println(textname);
				
				try {
					// incarcam fisierul pdf 
					//pd = PDDocument.load(pdfs.get(i));
					
					pd = PDDocument.load(pdfs.get(i).getAbsolutePath(), true);
					
					// se preia textul din pdf
					PDFTextStripper stripper = new PDFTextStripper("Unicode");
					String str = null;
					try {
						str = stripper.getText(pd);
					} catch (IOException e) {
						System.out.println("Eroare la extragere");
						str = null;
					}
					
					if (str != null) {
					
						String[] lines = str.split("\n");
						String new_text = new String();
						
						for (int j = 0; j < lines.length; j++) {
							String line = lines[j];
							
							String text = new String();
							if (line.contains("Societatea Comercial")) {
								//j++;
								//text += line + "\n";
								
								while(!line.matches("^[(][0-9/.]*[)]$")) {
									if (line.length() > 3 && !line.contains("MONITORUL OFICIAL AL ROM")) 
										text += line + "\n";
									
									j++;
									// test the end of file
									if (j >= lines.length) {
										line = "";
										break;
									}
									
									line = lines[j];
									
								}
								text += line + "\n";
								
								// if you have to index the file then create a new solrinputdocument
								// and add fields, for each paragraph from a file is created a new index
								if (args[1].equals("index")) {
									SolrInputDocument doc = new SolrInputDocument();
									doc.addField("cat", "mof");
									doc.addField("id", textname + "_" + i + "_" + index);
									index++;
									doc.addField("features", text);
									try {
										server.add(doc);
									} catch (SolrServerException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								new_text += text + "\n";
							}
						}
						
						new_text = correctDiact(new_text);
						
						if (args[1].equals("text")) {
							PrintWriter writer = new PrintWriter(textname, "UTF-8");
							
							writer.write(new_text);
							writer.close();
						} else if (args[1].equals("index")) {
							try {
								server.commit();
							} catch (SolrServerException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						
	//				    // Get the Java runtime
	//				    Runtime runtime = Runtime.getRuntime();
	//				    // Run the garbage collector
	//				    runtime.gc();
	//				    // Calculate the used memory
	//				    long memory = runtime.totalMemory() - runtime.freeMemory();
	//				    System.out.println("Used memory is bytes: " + memory);
	//				    System.out.println("Used memory is megabytes: "
	//				        + bytesToMegabytes(memory));
	//				
					}
					
					if (pd != null) {
						pd.close();
					}
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		} else {
			System.out.println("No such path");
		}

	}
	
	
	public static String correctDiact(String incorect) {
		Map<Character, Character> charReplacementMap = new HashMap<Character, Character>();
		charReplacementMap.put('∫', 'ș');
		charReplacementMap.put('„', 'ă');
		charReplacementMap.put('˛', 'ț');
		charReplacementMap.put('Ó', 'î');
		charReplacementMap.put('‚', 'â');
		charReplacementMap.put('Œ', 'Î');
		//charReplacementMap.put('î', '')
		charReplacementMap.put('√', 'Ă');
		charReplacementMap.put('™', 'Ș');
		charReplacementMap.put('¬', 'Â');
		
		StringBuilder builder = new StringBuilder();

		for (char currentChar : incorect.toCharArray()) {
		    Character replacementChar = charReplacementMap.get(currentChar);
		    builder.append(replacementChar != null ? replacementChar : currentChar);
		}

		String newString = builder.toString();
		return newString;
	}

}
