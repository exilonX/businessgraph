<?php
    error_reporting(-1);
    ini_set('display_errors', 'On');

    /* Set internal character encoding to UTF-8 */
    header('Content-type: text/plain; charset=utf-8');


    function correctDiacritic() {
        $phrase  = "You should eat fruits, vegetables, and fiber every day.";
        $healthy = array("fruits", "vegetables", "fiber");
        $yummy   = array("pizza", "beer", "ice cream");
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    $local_ip = "localhost";
    $port = "8080";
    
    $id = $_GET["id"];
    
    
    $address = "http://" . $local_ip . ":" . $port . "/GraphService/TestServlet";
    $address_get = $address . "?id=" . $id;
    
    //user curl to get page content
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $address_get);
    curl_setopt($ch, CURLOPT_HTTPGET, TRUE); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    $response = curl_exec($ch);
    curl_close($ch);

    // $response = utf8_encode($response);

    $response_a = json_decode((string)$response, true);
    $filename = generateRandomString(20);
    $file = "temp/" . $filename;



    file_put_contents($file, $response_a["graphjson"]);
    $response_a["graphjson"] = $file;

    echo json_encode($response_a);
    
?>
