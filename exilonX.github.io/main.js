function main() {
	
	// these are just some preliminary settings 
	var g = {
	    nodes: [],
	    edges: []
	};

	// Create new Sigma instance in graph-container div (use your div name here) 
	s = new sigma({
		graph: g,
		container: 'graph-container',
		renderer: {
			container: document.getElementById('graph-container'),
			type: 'canvas'
		},
		settings: {
			defaultNodeColor: '#ec5148'
		}
	});

	var files = {"files": [
		{ "filename":"jsonfile/TOT_1_206.json"},
		{ "filename":"jsonfile/TOT_2_148.json"},
		{ "filename":"jsonfile/TOT_3_133.json"},
		{ "filename":"jsonfile/TOT_4_128.json"},
		{ "filename":"jsonfile/TOT_5_110.json"},
		{ "filename":"jsonfile/TOT_6_96.json"},
		{ "filename":"jsonfile/TOT_7_84.json"},
		{ "filename":"jsonfile/TOT_8_79.json"},
		{ "filename":"jsonfile/TOT_9_77.json"},
		]};

	loadGraf(files['files'][0]['filename']);

	var maxLen = files['files'].length;
	var curent = 0;

	var buton = document.getElementById("stopforce");
	buton.addEventListener("click", stopForce, false);

	var next = document.getElementById("next");
   	next.addEventListener("click", nextGraf, false);

   	var previous = document.getElementById("previous");
   	previous.addEventListener("click", previousGraf, false);

  	//$("button").click(queryService);
  	$("#searchName").click(queryServiceName);


	function nextGraf() {
		curent = curent >= maxLen-1 ? 0 : curent+1;
		s.stopForceAtlas2();
		filename = files['files'][curent]['filename'];

		loadGraf(filename);
	}

	function previousGraf() {
		console.log(curent);
		curent = curent == 0 ? maxLen - 1 : curent - 1;
		s.stopForceAtlas2();
		filename = files['files'][curent]['filename'];
		console.log(curent);
		loadGraf(filename);
	}

	var respone;

	function queryService() {
		var name = "ION";
		console.log("INTRA AICISEA");

		var request = $.ajax({
			url: "http://localhost/graf/exilonX.github.io/proxy.php",
			type: "GET",
			data: { id : name },
			dataType: "json"
		});

		// if the request was successful
		request.done(function( msg ) {
			response = msg;
			console.log("RASPUNSUL ESTE " + response["graphjson"]);
		});

		// if the request failed
		request.fail(function( jqXHR, textStatus ) {
			alert( "Request failed: " + textStatus );
		});
	}


	function queryServiceName() {
		var name = $("#id").val();
		console.log("Intra aici in queryServiceName" + name);
		if (name.length >= 3) {
			var request = $.ajax({
				url: "http://localhost/graf/exilonX.github.io/proxy.php",
				type: "GET",
				data: { id : name },
				dataType: "json"
			});

			// if the request was successful
			request.done(function( msg ) {
				response = msg;
				console.log("RASPUNSUL ESTE " + response["graphjson"]);
				loadGraf(response["graphjson"]);
				$("#nrnodes").html("Numarul de noduri: " + response["numberNodes"]);
				$("#nredges").html("Numarul de muchii: " + response["numberEdges"]);
			});

			// if the request failed
			request.fail(function( jqXHR, textStatus ) {
				alert( "Request failed: " + textStatus );
			});
		} else {
			alert("Please type more than 3 caracters ");
		}

	}
}

// stop force atlas algorithm
function stopForce() {
	s.stopForceAtlas2();
}

function loadGraf(filename) {
	console.log("intra aici " + filename);
	// first you load a json with (important!) s parameter to refer to the sigma instance   
	var i = 0;
	sigma.parsers.json(
		filename,
		s,
		function() {
			// this below adds x, y attributes as well as size = degree of the node 
			var i,
	        nodes = s.graph.nodes(),
	        len = nodes.length;

			for (i = 0; i < len; i++) {
			    nodes[i].x = Math.random();
			    nodes[i].y = Math.random();
			    nodes[i].size = s.graph.degree(nodes[i].id);
			    nodes[i].color = '#' + Math.floor(Math.random() * 16777215).toString(16);
			}

			// Refresh the display:
			s.refresh();
			// ForceAtlas Layout
			s.startForceAtlas2();
		}
	);
}
