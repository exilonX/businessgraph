package abstr.graph;

public class AbstractNode {
	private int visited;

	public int getVisited() {
		return visited;
	}

	public void setVisited(int visited) {
		this.visited = visited;
	}
	
}
