package graphex;
import java.util.ArrayList;


public class ExtractNames {

	private String inputData;
	private ArrayList<String> outputNames;
	
	public ExtractNames() {
		this.inputData = new String();
		this.outputNames = new ArrayList<>();
	}
	
	public ExtractNames(String namesInput) {
		this.inputData = namesInput;
		this.outputNames = new ArrayList<>();
	}
	
	public void setInputData(String input) {
		this.inputData = input;
	}
	
	public String getInputData() {
		return this.inputData;
	}
	
	public ArrayList<String> extract() {
		ArrayList<String> names = testMultiple();
		//System.out.println(names);
		return names;
	}
	
	public ArrayList<String> lineByLineParser() {
		ArrayList<String> retData = new ArrayList<>();
		
		String[] lines = inputData.split("\n");
		StringBuilder item = new StringBuilder();
		int i = 1;
		for (String line : lines) {
//			if (inputData.contains("GĂTIN")) {
//				System.out.println("#" + line.trim() + "#");
//				
//			}
			String trimmed = line.trim();
			
			if (trimmed.contains(i + ".")) {
				i++;
				if (item.length() >= 2)
					retData.add(item.toString());
				item = new StringBuilder();
			}
			item.append(trimmed + "\n");
		}
		if (retData.size() == 0) {
			if (item.length() >= 2)
				retData.add(item.toString());
		}
		return retData;
	}
	
	/**
	 * Check if the inputData has multiple names to extract or just one
	 * @return  the number of expected names to extract
	 */
	private ArrayList<String> testMultiple() {
		//System.out.println(this.inputData);
		ArrayList<String> res = new ArrayList<>();
		
		ArrayList<String> parsed = lineByLineParser();
//		System.out.println("PARSED DATA");
//		for (int i = 0; i < parsed.size(); i++)
//			System.out.println(" XXXXXX " + parsed.get(i) + "XXXXXXXXXX");
		String[] splitted = parsed.toArray(new String[parsed.size()]);
		
//		if (inputData.contains("GĂTIN")) {
//			System.out.println("####" + inputData);
//			for (String s : splitted) {
//				System.out.println("$" + s + "$");
//			}
//		}
		
		for (int i = 0; i < splitted.length; i++) {
			if (!splitted[i].equals("\n")) {
				String[] splittedagain = splitted[i].split("\n");
				String first = null;
				for (int j = 0; j < splittedagain.length; j++) {
					if (splittedagain[j].length() >= 3) {
						first = splittedagain[j];
						break;
					}
				}
				
				if (first != null) {
					res.addAll(this.extractNames(first));
				}
			}
		}
		
		return res;
	}
	
	/**
	 * Function that extract the names from a paragraph that contains information
	 * @param id - from what to extract 1 - from fondatori 2- from admins
	 * @return - a list of names
	 */
	public static ArrayList<String> extractNames(String text) {
		// split the text after punctuation and white space to extract words
		String[] substrings = text.split("[\\p{IsPunctuation}\\p{IsWhite_Space}]+");
		
		int before = 0;
		int beforestart = 0;
		int cantake = 1;
		ArrayList<String> names = new ArrayList<>();
		String newname = new String();
		int i = 0;
		
		for (String s : substrings)
		{
			i++;
			
			int ok = 0;
			
			//System.out.println(s + "   " + s.matches("^\\p{javaUpperCase}.*$"));
		    if (s.matches("^\\p{javaUpperCase}.*$") && s.length() >= 3 && cantake == 1)
		    {
		    	//System.out.println("The text is : " + s);
		    	for (String notName : Constants.listOfNotNames) {
					if (s.equals(notName)) {
						ok = 1;
					}
		    	}
		    	
		    	if (ok == 0) {
					before = 1;
		    		newname += s + " ";
				}
		    } else if (before == 1 && s.matches("^\\p{javaUpperCase}$")) {
		    	newname += s + ". ";
		    	before = 1;
		    }
		    else if (before == 1) {
		    	if (!names.contains(newname))
		    		names.add(newname);
		    	newname = new String();
		    	before = 0;
		    	cantake = 0;
		    } else if(s.matches("[0-9]")) {
		    	
		    	cantake = 1;
		    } else {
		    	before = 0;
		    }
		}
		
		return names;
	}
	
}
