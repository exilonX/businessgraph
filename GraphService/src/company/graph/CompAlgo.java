package company.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import company.info.CompanyBasicInfo;


public class CompAlgo {
	CompGraph graph;
	
	public CompAlgo() {
		this.graph = new CompGraph();
	}
	
	public CompAlgo(CompGraph graph) {
		this.graph = graph;
	}
	
	/**
	 * Extract the connected components from a graph
	 * @param graf 
	 * @return
	 */
	public ArrayList<CompGraph> extractComponents() {
		HashMap<CompanyBasicInfo, CompNode> lista = this.graph.getGraph();
		
		ArrayList<CompGraph> componente = new ArrayList<>();
		
		Iterator<Entry<CompanyBasicInfo, CompNode>> it = lista.entrySet().iterator();
		

		while(it.hasNext()) {
			Entry<CompanyBasicInfo, CompNode> next = it.next();
			CompGraph componenta = new CompGraph();
			
			LinkedList<CompNode> toexpand = new LinkedList<>();
			toexpand.addLast(next.getValue());
			
			while(toexpand.size() != 0) {
				CompNode exp = toexpand.removeFirst();
				
				CompanyBasicInfo basic = new CompanyBasicInfo(exp.info.getNumeCompanie());
				int visited = lista.get(basic).getVisited();
				if (visited == 0) {
					
					Iterator<CompNode> it2 = exp.neighbours.iterator();
					
					while(it2.hasNext()) {
						CompNode vec = it2.next();
						componenta.addNode(vec.info);
						toexpand.addLast(vec);
					}
					
					lista.get(basic).setVisited(1);
				}
			}
			
			componente.add(componenta);
		}
		
		return componente;
	}
	
	/**
	 * Comparator class used for sorting a list of graphs after size
	 * @author Merca Ionel
	 *
	 */
	public class CompGraphComparator implements java.util.Comparator<CompGraph> {
		
		@Override
		public int compare(CompGraph arg0, CompGraph arg1) {
			int size1 = arg0.getGraph().size();
			int size2 = arg1.getGraph().size();
			return size2 - size1;
		}
		
	}
	
	/**
	 * return the nth biggest graph from the comp arrayList
	 * @param n  - the number of returned graphs
	 * @param comp - the list of graph to select from
	 * @return  - a linked list of the resulting graph the size will be n or less
	 */
	public LinkedList<CompGraph> getNMaxCompGraph(int n, ArrayList<CompGraph> comp) {
		LinkedList<CompGraph> max = new LinkedList<>();
		
		System.out.println("Inainte: " + comp.get(0).getGraph().size());
		Collections.sort(comp, new CompGraphComparator());
		System.out.println("Dupa " + comp.get(0).getGraph().size());
		
		n = n > comp.size() ? comp.size() : n;
		
		for (int i = 0; i < n; i++) {
			System.out.println(comp.get(i).getGraph().size());
			max.add(comp.get(i));
		}
		
		return max;
	}
	
	public void removeSingleNodes() {
		Iterator<Entry<CompanyBasicInfo, CompNode>> it = this.graph.getGraph().entrySet().iterator();
		
		while(it.hasNext()) {
			Entry<CompanyBasicInfo, CompNode> entr = it.next();
			if (entr.getValue().neighbours.size() == 0) {
				it.remove();
			}
		}
		
	}

}
