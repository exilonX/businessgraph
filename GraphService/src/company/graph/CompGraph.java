package company.graph;

import graphex.ExtractNames;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import bman.info.CompanyInfo;

import company.info.CompanyBasicInfo;

/**
 * Company graph class describes companies and relations between them
 * @author merca
 *
 */
public class CompGraph {
	private HashMap<CompanyBasicInfo, CompNode> graph;
	private HashMap<String, ArrayList<String>> nameCompRel;
	
	public CompGraph() {
		this.setGraph(new HashMap<CompanyBasicInfo, CompNode>());
		this.nameCompRel = new HashMap<>();
	}
	
	public CompGraph(HashMap<CompanyBasicInfo, CompNode> graph) {
		this.setGraph(graph);
	}
	
	/**
	 * Add a node to the Company Graph
	 * @param info - the info about the curent company
	 */
	public void addNode(CompanyInfo info) {
		// extract the names from the founders and admins
		ExtractNames extr = new ExtractNames(info.getFondatori());
		
		ArrayList<String> fondatori = extr.extract();
		extr.setInputData(info.getAdmins());
		ArrayList<String> admins = extr.extract();
		
		String compName = info.getNumeCompanie();
		CompanyBasicInfo basic = new CompanyBasicInfo(compName);
		
		// check if the node was already added to the graph if not
		// create a new node with the current company
		CompNode node = null;
		if (getGraph().containsKey(basic)) {
			node = getGraph().get(basic);
		} else {
			node = new CompNode(0, info);
			node.admins = admins;
			node.fondatori = fondatori;
		}
		
		ArrayList<String> names = new ArrayList<>();
		names.addAll(admins);
		names.addAll(fondatori);
		
		// for each name if it's already in the map of names and companies
		// add the curent company as a linked company else create a entry
		for(int i = 0; i < names.size(); i++) {
			ArrayList<String> myComp = null;
			if (this.nameCompRel.containsKey(names.get(i))) {
				myComp = this.nameCompRel.get(names.get(i));
				myComp.add(compName);
			} else {
				myComp = new ArrayList<String>();
				myComp.add(compName);
			}
			this.nameCompRel.put(names.get(i), myComp);
		}
		
		Iterator<String> itNames = names.iterator();
		ArrayList<String> companiesLinked = new ArrayList<>();
		
		// for each name that is implicated in this company get all 
		// the companies that have connections to that name and add a link
		// between those two companies, get a list of the connected
		// companies 
		while(itNames.hasNext()) {
			String name = itNames.next();
			if (nameCompRel.containsKey(name)) {
				ArrayList<String> myComps = nameCompRel.get(name);
				for (String comp : myComps) {
					if (!comp.equals(compName))
						companiesLinked.add(comp);
				}
			}
		}
		
		Set<String> uniqueLinked = new HashSet<>(companiesLinked);
		
		Iterator<String> itUnq = uniqueLinked.iterator();
		while(itUnq.hasNext()) {
			String comp = itUnq.next();
			CompNode compNode = this.graph.get(new CompanyBasicInfo(comp));
			
			node.neighbours.add(compNode);
			compNode.neighbours.add(node);
		}
		this.graph.put(basic, node);
	}
	
	public HashMap<CompanyBasicInfo, CompNode> getGraph() {
		return graph;
	}

	public void setGraph(HashMap<CompanyBasicInfo, CompNode> graph) {
		this.graph = graph;
	}
	
	
	
	

}
