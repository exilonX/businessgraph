package org.test.servlet;


import graphex.Export;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gephi.data.attributes.api.AttributeColumn;
import org.gephi.data.attributes.api.AttributeController;
import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.graph.api.GraphModel;
import org.gephi.statistics.plugin.GraphDistance;
import org.json.simple.JSONObject;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;






import org.openide.util.Lookup;

import bman.graph.Graph;
import bman.graph.GraphAlgo;
import bman.graph.Neo4jIOBMan;
import bman.info.BasicInfo;
import bman.graph.Node;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
//	public static String DB_PATH = "D:\\workspace1\\Graph\\graphneo.db";
	public static String DB_PATH = "D:\\workspace-jee\\GraphService\\graphneo.db3";
//	public static String DB_PATH = "D:\\workspace-jee\\GraphService\\graphneo.db2";
//	public static String DB_PATH = "D:\\workspace1\\Graph\\graphneo.db2";
	
	public static String DB_COMP_PATH = "D:\\workspace1\\Graph\\graphcompneo.db1";
	public ArrayList<String> labels = new ArrayList<String>();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TestServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		System.out.println("Intra in GET");

		String location = null;
		String caen = null;
		String year = null;
		String id = null;
		String type = "bman";

		if (request.getParameterMap().containsKey("location")) {
			location = request.getParameter("location");
			System.out.println(location);
		}

		if (request.getParameterMap().containsKey("caen")) {
			caen = request.getParameter("caen");
			System.out.println(caen);
		}

		if (request.getParameterMap().containsKey("year")) {
			year = request.getParameter("year");
			System.out.println(year);
		}

		if (request.getParameterMap().containsKey("id")) {
			id = request.getParameter("id");
			System.out.println(id);
		}

		if (request.getParameterMap().containsKey("type")) {
			type = request.getParameter("type");
			System.out.println(type);
		}

		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();

		response.setContentType("text/plain; charset=UTF-8");

		GraphDatabaseService graphdb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);

		if (id != null) {
			if (type.equals("bman")) {
				writeIdResponse(out, id, graphdb);
			}
		}
		
		System.out.println(labels);

		// if the user want to see the next connected component returned by a previous query
		if (request.getParameterMap().containsKey("next")) {
			if (labels.size() > 0) {
				String nextLabel = labels.get(0);
				writeNextResponse(out, nextLabel, graphdb);
			} else {
				writeErrorResponse(out);
			}
		}

		out.close();
		graphdb.shutdown();
		System.gc();

		/*if (url.isEmpty() || entity.isEmpty()) {
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			out.append("<response>");
			out.append("Nu s-au specificat un url sau o entitate valida!");
			out.append("</response>");
			out.close();
		} else {

			try {

			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Nu s-a putut calcula polaritatea entitatii! Este posibil ca serviciul web de la uaic sa nu functioneze!");
			}
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			out.append("<response>");
			out.append(polarity.toString());
			out.append("</response>");
			out.close();
		}*/
	}


	@SuppressWarnings("unchecked")
	private void writeResponse(PrintWriter out, String label, GraphDatabaseService graphdb, 
			Graph graf, String id) throws IOException {

		JSONObject responseJSON = new JSONObject();

		if (graf != null) {
//			System.out.println("The label is " + label.toString());
//			System.out.println(graf);
			JSONObject grafJson = Export.exportJSON(null, graf, id);
			responseJSON.put("graphjson", grafJson.toString());
			responseJSON.put("numberNodes", Neo4jIOBMan.getNumberofNodes(label.toString(), graphdb));
			responseJSON.put("numberEdges", Neo4jIOBMan.getNumberofEdges(label.toString(), graphdb));

			responseJSON.writeJSONString(out);
		} else {
			writeErrorResponse(out);
		}

//		System.out.println(responseJSON.toJSONString());
	}

	/**
	 * 
	 * @param label
	 * @param id
	 * @param nr
	 * @param responseJSON
	 * @param graphdb
	 */
	@SuppressWarnings("unchecked")
	private void writeNextGraphResponse(String label, String id, int nr,
			JSONObject responseJSON, GraphDatabaseService graphdb) {
//		System.out.println(label);
		Graph graf = Neo4jIOBMan.getComponentByLabel(label, graphdb);
		GraphAlgo algo = new GraphAlgo(graf);
		if (graf != null) {
//			System.out.println("The label is " + label.toString());
//			System.out.println(graf);
			JSONObject grafJson = Export.exportJSON(null, graf, id);
			responseJSON.put("graphjson" + nr, grafJson.toString());
			responseJSON.put("numberNodes" + nr, Neo4jIOBMan.getNumberofNodes(label.toString(), graphdb));
			responseJSON.put("numberEdges" + nr, Neo4jIOBMan.getNumberofEdges(label.toString(), graphdb));
			String interestId = getInterestId(id, graf);
//			System.out.println("=================== Interest id : " + interestId);
			
			if (interestId == null) {
				responseJSON.put("interestid" + nr, "none matched");
			} else {
				responseJSON.put("interestid" + nr, interestId);
				StringBuffer endId = new StringBuffer();
				Integer maxshort = maxShortestPath(graf, interestId, endId);
				responseJSON.put("maxshort"+ nr, maxshort);
				responseJSON.put("endnode" + nr, endId.toString());
			}
			ArrayList<String> arr = new ArrayList<>();
			algo.getProperties(arr, interestId);
			
			responseJSON.put("diameter" + nr, arr.get(0));
			responseJSON.put("avglen" + nr, arr.get(1));
			responseJSON.put("btwnode" + nr, arr.get(2));
			responseJSON.put("bestbtw" + nr, arr.get(3));
			responseJSON.put("rank" + nr, arr.get(4));
//			System.out.println("Inainte de floyd");
//			algo.floydWarshallWithPathRec();
//			System.out.println("Dupa floyd");
		}
	}


	/**
	 * Return the id of the node of interest
	 * @param id - the search pattern
	 * @param graph - the graph result
	 * @return - the id of the node that matched the search pattern
	 */
	public String getInterestId(String id, Graph graph) {
		HashMap<BasicInfo, Node> hashGraph = graph.getGraph();
		Iterator<Entry<BasicInfo, Node>> it = hashGraph.entrySet().iterator();

		while (it.hasNext()) {
			Entry<BasicInfo, Node> entr = it.next();
			if (entr.getKey().getIdentifier().toLowerCase().contains(id.toLowerCase())) {
				return entr.getKey().getIdentifier();
			}
		}

		return null;
	}

	/**
	 * Return the maximum Shortest Path of the graph <=>  the diameter of the graph
	 * @param graph
	 * @param idNode
	 * @param name
	 * @return
	 */
	public Integer maxShortestPath(Graph graph, String idNode, StringBuffer name) {
		System.out.println("In max shortest Path==========================");
		GraphAlgo algo = new GraphAlgo(graph);
		
		HashMap<String, Integer> dist = algo.dijkstra(idNode);
		Iterator<Entry<String, Integer>> it = dist.entrySet().iterator();
		Integer maxDist = Integer.MIN_VALUE;
		while(it.hasNext()) {
			Entry<String, Integer> entr = it.next();
			if (maxDist < entr.getValue()) {
				maxDist = entr.getValue();
				name.replace(0, name.length(), entr.getKey());
			}
		}

		return maxDist;
	}

	public void writeErrorResponse(PrintWriter out) throws IOException {
		JSONObject responseJSON = new JSONObject();
		responseJSON.put("numberNodes", "Nici un rezultat");
		responseJSON.writeJSONString(out);
	}


	public void writeIdResponse(PrintWriter out, String id, 
			GraphDatabaseService graphdb) throws IOException {
		labels = new ArrayList<>();
		Neo4jIOBMan.getComponentByBName(id, graphdb, labels);
		JSONObject responseJSON = new JSONObject();

		int size = labels.size() >= 5 ? 5 : labels.size();

		for (int i = 0; i < size; i++) {
			String label = labels.get(i);
//			System.out.println("AICI LABEL" + label);
			writeNextGraphResponse(label, id, i, responseJSON, graphdb);
		}
//		System.out.println(responseJSON.toJSONString());
		responseJSON.writeJSONString(out);
	}


	public void writeNextResponse(PrintWriter out, String label, 
			GraphDatabaseService graphdb) throws IOException {
		Graph graf = Neo4jIOBMan.getComponentByLabel(label, graphdb);
		writeResponse(out, label, graphdb, graf, null);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		out.println("Hello World");
	}

}
