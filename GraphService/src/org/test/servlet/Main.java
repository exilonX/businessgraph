package org.test.servlet;


import java.net.UnknownHostException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedList;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import bman.graph.Graph;
import bman.graph.GraphAlgo;
import bman.graph.Neo4jIOBMan;
import bman.info.CompanyInfo;

import com.itextpdf.text.log.SysoLogger;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import company.graph.CompAlgo;
import company.graph.CompGraph;
import company.graph.Neo4jIOComp;
import graphex.Constants;
import graphex.Export;


public class Main {
	public static String DB_PATH = "graphneo.db";
	public static String DB_COMP_PATH = "graphcompneo.db";
	
	public static void main(String[] args) {
		GraphDatabaseService graphdb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
		registerShutdownHook(graphdb);
		//CompGraph graf = Neo4jIOComp.getComponentByFilter(null, "2013", null, graphdb);
		Graph graf = Neo4jIOBMan.getComponentByFilter(null, null, "172", graphdb);
		//System.out.println();
		System.out.println(graf);

		//testQuery();
		
		//mongoDbData();
	}
	
	
	public static void testQuery() {
		GraphDatabaseService graphdb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_COMP_PATH);
		Label label = DynamicLabel.label( "comp10" );
		
		ExecutionEngine engine = new ExecutionEngine(graphdb);
		ExecutionResult result;
		
		try ( Transaction ignored = graphdb.beginTx() )
		{
		    //result = engine.execute( "match (n:comp10) return n" );
			result = engine.execute("start n=node(*) return count( distinct n)");
		    System.out.println(result.dumpToString());
		}
		
//		Iterator<Node> n_column = result.columnAs( "n" );
//		for ( Node node : IteratorUtil.asIterable( n_column ) )
//		{
//		    // note: we're grabbing the name property from the node,
//		    // not from the n.name in this case.
//		    System.out.println(node + ": " + node.getProperty( "name" ));
//		}
	}
	
	public static void mongoDbData() {
		MongoClient mongoClient;
		DB db;
		DBCollection coll;
		DBCursor cursor;
		DBObject crs;
		
		// the two graphs businessmen graph and companies graph
		Graph graf = new Graph();
		CompGraph compGraf = new CompGraph();
		CompanyInfo info;
		ArrayList<String> dbs = getDatabases();
		int totalsize = 0;
		for (String dbname : dbs) {
			try {
				//graf = new Graph();
				mongoClient = new MongoClient( "localhost" );
				db = mongoClient.getDB(dbname);
				System.out.println(db.getCollectionNames().toString());
				
				coll = db.getCollection("infiintari");
				cursor = coll.find();
				
				int i = 0;
				try {
					// for all the entries in the database add a new node to the
					// graph
					while(cursor.hasNext()) {
						crs = cursor.next();
						
						info = makeCompanyInfo(crs);
						
						//graf.addNode(info);
						compGraf.addNode(info);
						i++;
					}
				} finally {
					cursor.close();
				}
				
				System.out.println(dbname);
				System.out.println("GRAF SIZE " + graf.getGraph().size());
				System.out.println("GRAF COMP SIZE" + compGraf.getGraph().size());
				totalsize+= graf.getGraph().size();
				getMemoryUsage();
				
				//graf.exportJSON("mof.json");
				//System.out.println("Extract componente " + i);
	//			connectedComp(graf, "TOT");
	
	//			GraphModel grafModel = Export.makeGephiGraph(compGraf);
	//			Export.exportGEXF("/home/merca/businessGraph/graf.gexf", grafModel);
	//			Export.exportJSON("/home/merca/businessGraph/grafcomp.json", compGraf);
				
				
	//			Graph max_graf = graf.getMaxGraph(comp);
	//			System.out.println(max_graf);
	//			max_graf.exportJSON("mof.json");
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
		System.out.println(graf);
//		// delete single nodes from the graph
//		GraphAlgo algo = new GraphAlgo(graf);
//		System.out.println("GRAF SIZE " + graf.getGraph().size());
//		algo.removeSingleNodes();
//		System.gc();
//		getMemoryUsage();
		
		// export graf into a neo4j graph database
		//exportNeo(graf);
		exportNeoComp(compGraf);
		
//		CompAlgo algo = new CompAlgo(compGraf);
//		algo.removeSingleNodes();
//		System.out.println(compGraf.getGraph().size());
//		System.gc();
		
		// export the graph as gexf 
//		GraphModel grafModel = Export.makeGephiGraph(compGraf);
//		System.out.println("Dupa graf model");
//		Export.exportGEXF("/home/merca/businessGraph/CompGraf.gexf", grafModel);
		
//		grafModel = Export.makeGephiGraph(compGraf);
//		Export.exportGEXF("/home/merca/businessGraph/CompGraf.gexf", grafModel);
//		
		// get maximum connected componets
		//connectedComp(graf, "TOT");
		
		// get connected comp from company graph
		//connectedCompGraphComp(compGraf, "TOTCOMP");
		
//		System.out.println("TOtal size " + totalsize);
//		System.out.println("GRAF SIZE " + graf.getGraph().size());
//		System.out.println("GRAF COMP SIZE" + compGraf.getGraph().size());
	}
	

	/**
	 * Operation executed on the connected components of the graph
	 * @param graf
	 * @param year
	 */
	public static void connectedComp(Graph graf, String year) {
		GraphAlgo algo = new GraphAlgo(graf);
		
		// extract the connected components from the graph
		ArrayList<Graph> comp = algo.extractComponents();
		
		// extract n max components
		LinkedList<Graph> maxComp = algo.getNMaxGraph(50, comp);
		
		// export JSON for MAX COMP
		for (int i = 0; i < maxComp.size(); i++) {
			Graph aux = maxComp.get(i);
			String filename = year + "_" + i + "_" + aux.getGraph().size() + ".json";
			Export.exportJSON(filename, aux, null);
		}
	}
	
	/**
	 * Operation executed on the connected components of the graph
	 * @param graf
	 * @param year
	 */
	public static void connectedCompGraphComp(CompGraph graf, String year) {
		CompAlgo algo = new CompAlgo(graf);
		
		// extract the connected components from the graph
		ArrayList<CompGraph> comp = algo.extractComponents();
		
		// extract n max components
		LinkedList<CompGraph> maxComp = algo.getNMaxCompGraph(50, comp);
		
		// export JSON for MAX COMP
		for (int i = 0; i < maxComp.size(); i++) {
			CompGraph aux = maxComp.get(i);
			String filename = year + "_" + i + "_" + aux.getGraph().size() + ".json";
			Export.exportJSON(filename, aux);
		}
	}
	
	public static void getMemoryUsage() {
		Runtime runtime = Runtime.getRuntime();

	    NumberFormat format = NumberFormat.getInstance();

	    StringBuilder sb = new StringBuilder();
	    long maxMemory = runtime.maxMemory();
	    long allocatedMemory = runtime.totalMemory();
	    long freeMemory = runtime.freeMemory();

	    sb.append("free memory: " + format.format(freeMemory / 1024) + "<br/>");
	    sb.append("allocated memory: " + format.format(allocatedMemory / 1024) + "<br/>");
	    sb.append("max memory: " + format.format(maxMemory / 1024) + "<br/>");
	    sb.append("total free memory: " + format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024) + "<br/>");
	    System.out.println(sb.toString());
	}
	
	public static ArrayList<String> getDatabases() {
		ArrayList<String> arr = new ArrayList<>();
//		for (int i = 2011; i >= 2001; i--) {
//			
//			String name = "mofdb" + i;
//			arr.add(name);
//			
//		}
		arr.add("mofdb2014");
		return arr;
	}
	
	public static CompanyInfo makeCompanyInfo(DBObject crs) {
		CompanyInfo info = new CompanyInfo();
		info.setNumeCompanie(crs.get(Constants.Denumire).toString());
		info.setActivitatePrincipala(
				crs.get(Constants.ActivitatePrincipala).toString());
		info.setCapitalSocial(crs.get(Constants.Capital).toString());
		info.setCui(crs.get(Constants.CUI).toString());
		info.setDomeniuActivitate(
				crs.get(Constants.DomActivitate).toString());
		info.setFondatori(crs.get(Constants.Fondatori).toString());
		info.setNrOrd(crs.get(Constants.NrOrdine).toString());
		info.setSediuSocial(crs.get(Constants.SediuSocial).toString());
		info.setDurataFunc(crs.get(Constants.DurataFunct).toString());
		info.setAdmins(crs.get(Constants.Admin).toString());
		return info;
	}
	
	/**
	 * Export business man graph into neo4j database
	 * @param graph
	 */
	public static void exportNeo(Graph graph) {
		GraphDatabaseService graphDb;
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase( DB_PATH );
		registerShutdownHook(graphDb);
		Neo4jIOBMan.exportBManGraph(graph, graphDb);
	}
	
	/**
	 * Export company graph into neo4j database
	 * @param graph
	 */
	public static void exportNeoComp(CompGraph graph) {
		GraphDatabaseService graphDb;
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_COMP_PATH);
		registerShutdownHook(graphDb);
		Neo4jIOComp.exportCompGraph(graph, graphDb);
	}
	
	public static void registerShutdownHook(final GraphDatabaseService graphDb)	{
	    // Registers a shutdown hook for the Neo4j instance so that it
	    // shuts down nicely when the VM exits (even if you "Ctrl-C" the
	    // running application).
	    Runtime.getRuntime().addShutdownHook( new Thread()
			    {
			        @Override
			        public void run()
			        {
			            graphDb.shutdown();
			        }
			    } );
	}

	
}
