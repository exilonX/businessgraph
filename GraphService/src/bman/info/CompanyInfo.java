package bman.info;

import graphex.Constants;

import java.util.ArrayList;


/**
 * Class that contains information about a company, in most of the cases 
 * this class will be the edge between two nodes representing two business men  
 * @author Merca Ionel
 *
 */
public class CompanyInfo {

	private String numeCompanie;
	private String cui;
	private String nrOrd;
	private String sediuSocial;
	private String domeniuActivitate;
	private String activitatePrincipala;
	private String capitalSocial;
	private String fondatori;
	private String durataFunc;
	private String admins;
	
	/**
	 * Basic constructor initializes all the fields
	 */
	public CompanyInfo() {
		this.setNumeCompanie(new String());
		this.cui = new String();
		this.nrOrd = new String();
		this.sediuSocial = new String();
		this.domeniuActivitate = new String();
		this.activitatePrincipala = new String();
		this.capitalSocial = new String();
		this.fondatori = new String();
		this.durataFunc = new String();
		this.admins = new String();
	}
	
	/**
	 * Constructor that sets all the fields 
	 * @param nume -  the name of the company
	 * @param cui - the Unique registration code of the company
	 * @param nrord - the registration number 
	 * @param sediuSoc - the address of the company
	 * @param domActiv - the field of activity
	 * @param capital - the capital 
	 * @param fondatori - the founding members of the company
	 * @param activprin  -  main activity
	 * @param admins - the administrator of the company
	 * @param duratafunc - the duration of activity of the company
	 */
	public CompanyInfo(String nume, String cui, String nrord, String sediuSoc,
			String domActiv, String capital, String fondatori, String activprin,
			String admins, String duratafunc) {
		this.setNumeCompanie(nume.intern());
		this.cui = cui.intern();
		this.nrOrd = nrord.intern();
		this.sediuSocial = sediuSoc.intern();
		this.domeniuActivitate = domActiv.intern();
		this.activitatePrincipala = activprin.intern();
		//this.capitalSocial = capital.intern();
		//this.fondatori = fondatori.intern();
		//this.admins = admins.intern();
		this.durataFunc = duratafunc.intern();
	}
	
	
	/**
	 * Function that extract the names from a paragraph that contains information
	 * @param id - from what to extract 1 - from fondatori 2- from admins
	 * @return - a list of names
	 */
	public static ArrayList<String> extractNames(String text) {
		// split the text after punctuation and white space to extract words
		String[] substrings = text.split("[\\p{IsPunctuation}\\p{IsWhite_Space}]+");
		
		int before = 0;
		int beforestart = 0;
		int cantake = 1;
		ArrayList<String> names = new ArrayList<>();
		String newname = new String();
		int i = 0;
		for (String s : substrings)
		{
			i++;
			
			int ok = 0;
		    if (s.matches("^\\p{javaUpperCase}") && s.length() >= 3 && cantake == 1)
		    {
		    	for (String notName : Constants.listOfNotNames) {
					if (s.equals(notName)) {
						ok = 1;
					}
		    	}
		    	
		    	if (ok == 0) {
					before = 1;
		    		newname += s + " ";
				}
		    } else if (before == 1 && s.matches("^\\p{javaUpperCase}$")) {
		    	newname += s + ". ";
		    	before = 1;
		    }
		    else if (before == 1) {
		    	if (!names.contains(newname))
		    		names.add(newname);
		    	newname = new String();
		    	before = 0;
		    	cantake = 0;
		    } else if(s.matches("[0-9]")) {
		    	
		    	cantake = 1;
		    } else {
		    	before = 0;
		    }
		}
		
		return names;
	}
	
	public String getCui() {
		return cui;
	}
	public void setCui(String cui) {
		this.cui = cui;
	}
	public String getNrOrd() {
		return nrOrd;
	}
	public void setNrOrd(String nrOrd) {
		this.nrOrd = nrOrd;
	}
	public String getSediuSocial() {
		return sediuSocial;
	}
	public void setSediuSocial(String sediuSocial) {
		this.sediuSocial = sediuSocial;
	}
	public String getDomeniuActivitate() {
		return domeniuActivitate;
	}
	public void setDomeniuActivitate(String domeniuActivitate) {
		this.domeniuActivitate = domeniuActivitate;
	}
	public String getActivitatePrincipala() {
		return activitatePrincipala;
	}
	public void setActivitatePrincipala(String activitatePrincipala) {
		this.activitatePrincipala = activitatePrincipala;
	}
	public String getCapitalSocial() {
		return capitalSocial;
	}
	public void setCapitalSocial(String capitalSocial) {
		this.capitalSocial = capitalSocial;
	}
	public String getFondatori() {
		return fondatori;
	}
	public void setFondatori(String fondatori) {
		this.fondatori = fondatori;
	}

	public String getDurataFunc() {
		return durataFunc;
	}

	public void setDurataFunc(String durataFunc) {
		this.durataFunc = durataFunc;
	}

	public String getAdmins() {
		return admins;
	}

	public void setAdmins(String admins) {
		this.admins = admins;
	}
	
	@Override
	public String toString() {
		String result = new String();
		result += "Denumire Societate : " + this.getNumeCompanie() + "\n";
		result += "CUI Societate : " + this.cui + "\n";
		result += "Nr ordine Reg Comertului : " + this.nrOrd + "\n";
		result += "Sediu Social: " + this.sediuSocial + "\n";
		result += "Domeniu Principal de activitate: " + this.domeniuActivitate + "\n";
		result += "Activitatea Principala: " + this.activitatePrincipala + "\n";
		result += "Capital Social: " + this.capitalSocial + "\n";
		result += "Fondatori: " + this.fondatori + "\n";
		result += "Administratori: " +  this.admins + "\n";
		result += "Durata de Functionare: " + this.durataFunc + "\n";
		return result;
	}

	public String getNumeCompanie() {
		return numeCompanie;
	}

	public void setNumeCompanie(String numeCompanie) {
		this.numeCompanie = numeCompanie;
	}

}
