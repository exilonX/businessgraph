package bman.info;

import abstr.info.AbstractBasicInfo;

public class BasicInfo extends AbstractBasicInfo {

	private String cnp;
	private String adresa;
	private String statut;
	
	public BasicInfo() {
		super();
		this.cnp = new String();
		this.adresa = new String();
		this.setStatut(new String());
	}
	
	public BasicInfo(String nume) {
		super(nume);
		this.cnp = new String();
		this.adresa = new String();
		this.setStatut(new String());
	}
	
	public String getCnp() {
		return cnp;
	}
	
	public void setCnp(String cnp) {
		this.cnp = cnp;
	}
	
	public String getAdresa() {
		return adresa;
	}
	
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		String res = new String();
		res += this.getIdentifier() + " ";
		res += this.statut;
		return res;
	}

}
