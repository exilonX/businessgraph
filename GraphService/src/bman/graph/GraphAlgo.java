package bman.graph;


import graphex.Export;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import javax.websocket.RemoteEndpoint.Basic;

import org.gephi.data.attributes.api.AttributeColumn;
import org.gephi.data.attributes.api.AttributeController;
import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.graph.api.GraphModel;
import org.gephi.statistics.plugin.GraphDistance;
import org.openide.util.Lookup;

import com.itextpdf.text.log.SysoLogger;

import bman.info.BasicInfo;
import bman.info.CompanyInfo;

/**
 * Implementation of various graph algorithms
 * @author Merca Ionel
 *
 */
public class GraphAlgo {
	private Graph graph;
	private HashMap<String, Integer> nodeIndex;

	public GraphAlgo() {
		this.graph = new Graph();
	}

	public GraphAlgo(Graph graf) {
		this.graph = graf;
	}

	/**
	 * Extract the connected components from a graph
	 * @return - list of all the connected components
	 */
	public ArrayList<Graph> extractComponents() {
		HashMap<BasicInfo, Node> lista = this.graph.getGraph();
		ArrayList<Graph> componente = new ArrayList<>();
		Iterator<Map.Entry<BasicInfo, Node>> it = lista.entrySet().iterator();

		while(it.hasNext()) {
			Entry<BasicInfo, Node> next = it.next();
			Graph componenta = new Graph();

			LinkedList<Node> toexpand = new LinkedList<>();
			toexpand.addLast(next.getValue());

			while(toexpand.size() != 0) {
				Node exp = toexpand.removeFirst();

				BasicInfo basic = new BasicInfo(exp.getbManName());
				int visited = lista.get(basic).getVisited();
				if (visited == 0) {
					// iterate through all the linked nodes
					Iterator<Entry<Node, CompanyInfo>> it2 = 
							exp.getColaborations().entrySet().iterator();

					while(it2.hasNext()) {
						Entry<Node, CompanyInfo> vec = it2.next();
						componenta.addNode(vec.getValue());
						toexpand.addLast(vec.getKey());
					}

					lista.get(basic).setVisited(1);
				}
			}
			if (componenta.getGraph().size() > 0)
				componente.add(componenta);
		}

		return componente;
	}

	/**
	 * returns the graph with the maximum size
	 * @param comp
	 * @return
	 */
	public Graph getMaxGraph(ArrayList<Graph> comp) {
		int k = 0;
		int max_size = -1;
		Graph max_graf = null;

		for (int i = 0; i < comp.size(); i++) {
			int size = comp.get(i).getGraph().size();
			if (size > 2) {
				k++;
				if (size > max_size ) {
					max_size = size;
					max_graf = comp.get(i);
				}
			}
		}

		System.out.println("K : " + k);
		System.out.println(comp.size());

		return max_graf;
	}

	/**
	 * Comparator class used for sorting a list of graphs after size
	 * @author Merca Ionel
	 *
	 */
	public class GraphComparator implements java.util.Comparator<Graph> {

		@Override
		public int compare(Graph arg0, Graph arg1) {
			int size1 = arg0.getGraph().size();
			int size2 = arg1.getGraph().size();
			return size2 - size1;
		}

	}

	/**
	 * return the nth biggest graph from the comp arrayList
	 * @param n  - the number of returned graphs
	 * @param comp - the list of graph to select from
	 * @return  - a linked list of the resulting graph the size will be n or less
	 */
	public LinkedList<Graph> getNMaxGraph(int n, ArrayList<Graph> comp) {
		LinkedList<Graph> max = new LinkedList<>();

		System.out.println("Inainte: " + comp.get(0).getGraph().size());
		Collections.sort(comp, new GraphComparator());
		System.out.println("Dupa " + comp.get(0).getGraph().size());

		n = n > comp.size() ? comp.size() : n;

		for (int i = 0; i < n; i++) {
			max.add(comp.get(i));
		}

		return max;
	}

	/**
	 * Check if a graph is complete, if exists a edge between any two nodes
	 * @return
	 */
	public boolean checkComplete() {
		int n = graph.getGraph().size();
		Iterator<Entry<BasicInfo, Node>> it = this.graph.getGraph().entrySet().iterator();

		while(it.hasNext()) {
			Entry<BasicInfo, Node> entr = it.next();
			if (entr.getValue().getColaborations().size() != n) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Remove nodes that don't have any connections
	 */
	public void removeSingleNodes() {
		Iterator<Entry<BasicInfo, Node>> it = this.graph.getGraph().entrySet().iterator();

		while(it.hasNext()) {
			Entry<BasicInfo, Node> entr = it.next();
			if (entr.getValue().getColaborations().size() == 0) {
				it.remove();
			}
		}
	}

	/**
	 * Compute the sortest paths from the starting node to all the other nodes
	 * @param idNode - the id of the node
	 * @return a list of all the shortest paths
	 */
	public HashMap<String, Integer> dijkstra(String idNode) {
		//mark all nodes as unvisited and init tentative distance
		Iterator<Entry<BasicInfo, Node>> it = this.graph.getGraph().entrySet().iterator();
		HashMap<String, Integer> distances = new HashMap<>();

		while(it.hasNext()) {
			Entry<BasicInfo, Node> entr = it.next();
			if (entr.getKey().getIdentifier().equals(idNode)) {
				distances.put(entr.getKey().getIdentifier(), 0);
			} else {
				distances.put(entr.getKey().getIdentifier(), Integer.MAX_VALUE);
			}
			entr.getValue().setVisited(0);
		}

		LinkedList<Node> devizitat = new LinkedList<Node>();
		Node startNode = this.graph.getGraph().get(new BasicInfo(idNode));
		System.out.println("Dupa init");
		if (startNode != null) {
			devizitat.push(startNode);

			while(devizitat.size() > 0) {
				Node next = devizitat.pollFirst();

				if (next.getVisited() == 0) {
					HashMap<Node, CompanyInfo> neighbours = next.getColaborations();

					Iterator<Entry<Node, CompanyInfo>> itNeigh =
							neighbours.entrySet().iterator();
					int curentDist = distances.get(next.getbManName());

					while(itNeigh.hasNext()) {
						Entry<Node, CompanyInfo> entr = itNeigh.next();
						String id = entr.getKey().getbManName();

						if (entr.getKey().getVisited() == 0) {

							if (distances.containsKey(id)) {
								int nextDist = curentDist + 1;
								if (distances.get(id) > nextDist) {
									distances.put(id, nextDist);
								}
							}

							devizitat.push(entr.getKey());
						}

					}

					next.setVisited(1);

				}
			}

		} else {
			return null;
		}

		return distances;
	}


	/**
	 * Compute the diameter of the graph by apply dijkstra for each node and 
	 * computing the maximum shortest path lenght
	 * @return
	 */
	public void computeProperties(ArrayList<String> result) {
		HashMap<BasicInfo, Node> graf = this.graph.getGraph();
		Iterator<Entry<BasicInfo, Node>> it = graf.entrySet().iterator();
		Integer sum = 0;
		Integer nr = 0;
		Integer diameter;
		Double avgLen;
		Integer maxPathLen = Integer.MIN_VALUE;

		while(it.hasNext()) {
			Entry<BasicInfo, Node> entr = it.next();

			HashMap<String, Integer> dijkRes = dijkstra(entr.getKey().getIdentifier());

			Iterator<Entry<String, Integer>> itRes = dijkRes.entrySet().iterator();

			while(itRes.hasNext()) {
				Entry<String, Integer> val = itRes.next();
				sum += val.getValue();
				nr += 1;
				if (val.getValue() > maxPathLen) {
					maxPathLen = val.getValue();
				}	
			}
		}

		diameter = maxPathLen;
		avgLen = (sum * 1.0) / nr;

		result.add(Integer.toString(diameter));
		result.add(Double.toString(avgLen));
	}


	/**
	 * Floyd Wharshall algorithm used to compute betweenness centrality
	 */
	public Integer[][] floydWarshallWithPathRec() {
		// map that associates each node an index
		nodeIndex = new HashMap<String, Integer>();

		int size = this.graph.getGraph().size();
		Integer distances[][] = new Integer[size][size];
		ArrayList paths[][] = new ArrayList[size][size];
		Integer numberPaths[][] = new Integer[size][size];

		// map an index to each node
		Iterator<BasicInfo> it = graph.getGraph().keySet().iterator();
		int index = 0;

		while(it.hasNext()) {
			String name = it.next().getIdentifier();
			nodeIndex.put(name, index);
			System.out.println(name + "  " + index);
			index++;
		}

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (i == j)
					distances[i][j] = 0;
				else
					distances[i][j] = 10000;
				paths[i][j] = new ArrayList<>();
				paths[i][j].add(-1);
				numberPaths[i][j] = 0;
			}
		}

		Iterator<Entry<BasicInfo, Node>> itG = graph.getGraph().entrySet().iterator();

		while(itG.hasNext()) {
			Entry<BasicInfo, Node> entry = itG.next();
			Integer startIndex = nodeIndex.get(entry.getKey().getIdentifier());

			Iterator<Entry<Node, CompanyInfo>> itNeigh = 
					entry.getValue().getColaborations().entrySet().iterator();

			while(itNeigh.hasNext()) {
				Entry<Node, CompanyInfo> entr = itNeigh.next();
				String name = entr.getKey().getbManName();
				Integer endIndex = nodeIndex.get(name);
				distances[startIndex][endIndex] = 1;
				numberPaths[startIndex][endIndex] = 1;
				paths[startIndex][endIndex] = new ArrayList<Integer>();
				paths[startIndex][endIndex].add(endIndex);
			}
		}

		for (int k = 0; k < size; k++) {
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					if (distances[i][k] + distances[k][j] < distances[i][j]) {
						distances[i][j] = distances[i][k] + distances[k][j];

						paths[i][j] = new ArrayList<Integer>();
						paths[i][j].add(k);
						numberPaths[i][j] = 1;
					} else if (distances[i][k] + distances[k][j] == distances[i][j]
							&& k != j && k != i) {
						numberPaths[i][j]++;
						paths[i][j].add(k);

					}
				}
			}
		}

		System.out.println("Max size :" + size);
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (paths[i][j] != null)
					System.out.print(distances[i][j] + " , " + numberPaths[i][j] + " , " + paths[i][j].toString() + "  ");
				else {
					System.out.print(distances[i][j] + " , " + numberPaths[i][j] + " , " + null + "  ");
				}
			}
			System.out.println();
		}

		//ArrayList<ArrayList<Integer>> pat = getPath(0, 1, paths);
		//System.out.println(pat.toString());

		return distances;
	}

	/**
	 * 
	 * @param u
	 * @param v
	 * @param paths
	 * @return
	 */
	public ArrayList<ArrayList<Integer>> getPath(Integer u, Integer v, ArrayList[][] paths) {
		ArrayList<ArrayList<Integer>> allpaths = new ArrayList<ArrayList<Integer>>();
		System.out.println("Path between " + u + "  " + v);

		if (paths[u][v] == null) {
			return null;
		}

		ArrayList<Integer> vect = paths[u][v];
		int size = vect.size();

		for(int i = 0; i < size; i++) {
			int k = vect.get(i);
			if (k == -1) {
				ArrayList<Integer> arr = new ArrayList<>();
				arr.add(u);
				arr.add(v);
				allpaths.add(arr);
			} else {
				ArrayList<ArrayList<Integer>> paths_U_K = getPath(u, k, paths);
				ArrayList<ArrayList<Integer>> paths_K_V = getPath(k, v, paths);

				for (int i1 = 0; i1 < paths_U_K.size(); i1++) {
					ArrayList<Integer> path_U_K = paths_U_K.get(i1);
					path_U_K.remove(path_U_K.size() - 1);
					for (int j1 = 0; j1 < paths_K_V.size(); j1++) {
						ArrayList<Integer> path_K_V = paths_K_V.get(j1);
						ArrayList<Integer> nou = new ArrayList<>();
						nou.addAll(path_U_K);
						nou.addAll(path_K_V);
						allpaths.add(nou);
					}
				}

			}
		}

		return allpaths;
	}

	/**
	 * 
	 * @param result
	 */
	public void getProperties(ArrayList<String> result, String interestName) {
		Integer[][] distances = floydWarshallWithPathRec();
		int size = distances.length;

		int diameter = Integer.MIN_VALUE;
		int sum = 0;
		int count = 0;

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (i != j) {
					sum += distances[i][j];
					count++;
					if (distances[i][j] > diameter) {
						diameter = distances[i][j];
					}
				}
			}
		}

		Double avgLen = (1.0 * sum) / count;
		DecimalFormat df = new DecimalFormat("#.#####");
		result.add(Integer.toString(diameter));
		result.add(df.format(avgLen));
		Double btw = getBetweennessNode(interestName);
		result.add(df.format(btw));
		String bestBtw = maxBetweennessNode();
		
		result.add(bestBtw);
		Integer rank = getRankNode(interestName);
		result.add(rank.toString());
	}

	public Double getBetweennessNode(String node) {
		String name = Export.correctDiact(node);

		GraphModel model = Export.makeGephiGraph(this.graph);
		GraphDistance distance = new GraphDistance();

		AttributeController ac = Lookup.getDefault().lookup(AttributeController.class);
		AttributeModel attModel = ac.getModel();

		distance.execute(model, attModel);

		AttributeColumn col = attModel.getNodeTable().getColumn(GraphDistance.BETWEENNESS);
		org.gephi.graph.api.Graph gepgraph = model.getGraph();

		//Iterate over values
		for (org.gephi.graph.api.Node n : gepgraph.getNodes()) {
			if (n.getNodeData().getLabel().equals(name)) {
				Double centrality = (Double)n.getNodeData().getAttributes().getValue(col.getIndex());
				return centrality;
			}
		}
		
		return 0.0;
	}
	
	public Integer getRankNode(String node) {
		String name = Export.correctDiact(node);

		GraphModel model = Export.makeGephiGraph(this.graph);
		GraphDistance distance = new GraphDistance();

		AttributeController ac = Lookup.getDefault().lookup(AttributeController.class);
		AttributeModel attModel = ac.getModel();

		distance.execute(model, attModel);

		AttributeColumn col = attModel.getNodeTable().getColumn(GraphDistance.BETWEENNESS);
		org.gephi.graph.api.Graph gepgraph = model.getGraph();
		
		Double nodeCentr = 0.0;
		Integer rank = gepgraph.getNodeCount();
		//Iterate over values
		for (org.gephi.graph.api.Node n : gepgraph.getNodes()) {
			if (n.getNodeData().getLabel().equals(name)) {
				nodeCentr = (Double)n.getNodeData().getAttributes().getValue(col.getIndex());
			}
		}
		
		for (org.gephi.graph.api.Node n : gepgraph.getNodes()) {
			if (!n.getNodeData().getLabel().equals(name)) {
				Double central = (Double)n.getNodeData().getAttributes().getValue(col.getIndex());
				if (central <= nodeCentr) {
					rank--;
				}
			}
		}
		
		return rank;
	}
	

	public String maxBetweennessNode() {

		GraphModel model = Export.makeGephiGraph(this.graph);
		GraphDistance distance = new GraphDistance();

		AttributeController ac = Lookup.getDefault().lookup(AttributeController.class);
		AttributeModel attModel = ac.getModel();

		distance.execute(model, attModel);

		AttributeColumn col = attModel.getNodeTable().getColumn(GraphDistance.BETWEENNESS);
		org.gephi.graph.api.Graph gepgraph = model.getGraph();
		
		Double maxBetween = Double.MIN_VALUE;
		String maxNode = new String();
		
		//Iterate over values
		for (org.gephi.graph.api.Node n : gepgraph.getNodes()) {
			Double centrality = (Double)n.getNodeData().getAttributes().getValue(col.getIndex());
			if (centrality > maxBetween) {
				maxBetween = centrality;
				maxNode = n.getNodeData().getLabel();
			}
		}
		
		return maxNode;
	}


	public HashMap<String, Integer> getNodeIndex() {
		return nodeIndex;
	}

	public void setNodeIndex(HashMap<String, Integer> nodeIndex) {
		this.nodeIndex = nodeIndex;
	}



}
