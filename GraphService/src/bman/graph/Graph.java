package bman.graph;

import graphex.Constants;
import graphex.ExtractNames;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import bman.info.BasicInfo;
import bman.info.CompanyInfo;


public class Graph {

	private HashMap<BasicInfo, Node> graph;
	
	
	public Graph() {
		this.setGraph(new HashMap<BasicInfo, Node>());
		
	}

	public HashMap<BasicInfo, Node> getGraph() {
		return graph;
	}

	public void setGraph(HashMap<BasicInfo, Node> graph) {
		this.graph = graph;
	}
	
	/**
	 * Function that adds a node to the graph, 
	 * @param info - the information about a company from the database
	 */
	public void addNode(CompanyInfo info) {
		ExtractNames extr = new ExtractNames(info.getFondatori());
		
		ArrayList<String> fondatori = extr.extract();
		extr.setInputData(info.getAdmins());
		ArrayList<String> admins = extr.extract();
		
		/// list of basic info about the admins and founders 
		ArrayList<BasicInfo> basiclist = new ArrayList<>();
 		
		for (int i = 0; i < fondatori.size(); i++) {
			BasicInfo basic = new BasicInfo();
			basic.setStatut(Constants.statusFound);
			basic.setIdentifier(fondatori.get(i));
			basiclist.add(basic);
		}
		
		for (int i = 0; i < admins.size(); i++) {
			int ok = 0;
			// if the name was already inserted in the basiclist then update
			// the status
			for (int j = 0; j < basiclist.size(); j++) {
				if (admins.get(i).equals(basiclist.get(j).getIdentifier())) {
					basiclist.get(j).setStatut(Constants.statusFA);
					ok = 1;
					break;
				}
			}
			// add a new basic info
			if (ok != 1) {
				BasicInfo basic = new BasicInfo();
				basic.setStatut(Constants.statusAdmin);
				basic.setIdentifier(admins.get(i));
				basiclist.add(basic);
			}
		}
		
//		System.out.println("Inceput : ");
//		System.out.println("Fondatori " + fondatori);
//		System.out.println("ADMINS " + admins);
//		System.out.println(" INFO FONDATORI " + info.getFondatori());
//		System.out.println(" INFO ADMINS " + info.getAdmins());
//		System.out.println(" BASIC LIST CU TOTI " + basiclist);
//		
		
		// for all basic elements add a link between the two nodes
		for (int i = 0; i < basiclist.size(); i++) {
			BasicInfo basic = basiclist.get(i);
			Node node = null;
			if (graph.containsKey(basic)) {
				node = graph.get(basic);
				node.add_owned(info);
				
			} else {
				node = new Node();
				node.setbManName(basic.getIdentifier());
				node.add_owned(info);
			}
			
			for (int j = 0; j < basiclist.size(); j++) {
				if (j != i) {
					BasicInfo b = basiclist.get(j);
					if (graph.containsKey(b)) {
						Node n = graph.get(b);
						node.add_colaboration(n, info);
					} else {
						Node n = new Node();
						n.setbManName(b.getIdentifier());
						n.add_owned(info);
						this.graph.put(b, n);
						node.add_colaboration(n, info);
					}
				}
			}
			
			this.graph.put(basic, node);
		}
	}
	
	
	
	@Override
	public String toString() {
		Iterator<Entry<BasicInfo, Node>> it = this.graph.entrySet().iterator();
		String result = new String();
		
		while(it.hasNext()) {
			Entry<BasicInfo, Node> next = it.next();
			result += next.getKey().getIdentifier() + "  { ";
			Node node = next.getValue();
			Iterator<Entry<Node, CompanyInfo>> itr = 
					node.getColaborations().entrySet().iterator();
			while(itr.hasNext()) {
				Node n = itr.next().getKey();
				result += n.getbManName() + " , ";
			}
			result += " } \n";
		}
		return result;
	}
}
