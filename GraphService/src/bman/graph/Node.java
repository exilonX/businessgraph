package bman.graph;


import java.util.ArrayList;
import java.util.HashMap;

import bman.info.CompanyInfo;

/**
 * A node in the graph is defined by the name of the business man the owned 
 * companies and the collaborations with other business men (the list of 
 * neighbours)
 * @author merca
 *
 */
public class Node {
	private String bManName;
	private ArrayList<CompanyInfo> owned;
	private HashMap<Node, CompanyInfo> collaborations;
	private int visited;
	private int distance;
	
	public Node() {
		this.setbManName(new String());
		this.setOwned(new ArrayList<CompanyInfo>());
		this.setColaborations(new HashMap<Node, CompanyInfo>());
		this.setVisited(0);
		this.setDistance(0);
	}
	
	public Node(String bManName) {
		this.setbManName(bManName);
		this.setOwned(new ArrayList<CompanyInfo>());
		this.setColaborations(new HashMap<Node, CompanyInfo>());
		this.setVisited(0);
		this.setDistance(0);
	}

	public ArrayList<CompanyInfo> getOwned() {
		return owned;
	}

	public void setOwned(ArrayList<CompanyInfo> owned) {
		this.owned = owned;
	}

	public HashMap<Node, CompanyInfo> getColaborations() {
		return collaborations;
	}

	public void setColaborations(HashMap<Node, CompanyInfo> colaborations) {
		this.collaborations = colaborations;
	}

	public String getbManName() {
		return bManName;
	}

	public void setbManName(String bManName) {
		this.bManName = bManName;
	}
	
	@Override
	public int hashCode() {
		int hash=7;
		for (int i=0; i < this.bManName.length(); i++) {
		    hash = hash*31 + this.bManName.charAt(i);
		}
		return hash;
	}
	
	public void add_owned(CompanyInfo info) {
		this.owned.add(info);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this.bManName.equals(((Node)obj).bManName)) {
			return true;
		}
		return false;
	}
	
	public void add_colaboration(Node node, CompanyInfo info) {
		collaborations.put(node, info);
	}

	public int getVisited() {
		return visited;
	}

	public void setVisited(int visited) {
		this.visited = visited;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

}
