package company.info;

import abstr.info.AbstractBasicInfo;


public class CompanyBasicInfo extends AbstractBasicInfo {
	
	public CompanyBasicInfo() {
		super();
	}
	
	public CompanyBasicInfo(String company) {
		super(company);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		return "Company Name : " + this.getIdentifier() + "\n" ;
	}

}
