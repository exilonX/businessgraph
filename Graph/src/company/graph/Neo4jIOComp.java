package company.graph;

import graphex.Constants;
import graphex.CypherQueries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.helpers.collection.IteratorUtil;

import bman.graph.Graph;
import bman.info.CompanyInfo;
import company.info.CompanyBasicInfo;

/**
 * Neo4j Company Graph Input Output
 * @author merca
 *
 */
public class Neo4jIOComp {
	
	/**
	 * Export company Graph into the neo4j Database
	 * @param graph - the graph to be exported
	 * @param graphDb - the neo4j graph service
	 */
	public static void exportCompGraph(CompGraph graph, GraphDatabaseService graphDb) {
		HashMap<CompanyBasicInfo, CompNode> lista = graph.getGraph();
		Iterator<Map.Entry<CompanyBasicInfo, CompNode>> it = lista.entrySet().iterator();
		int i = 0;
		
		// iterate through all the nodes in the graph
		while(it.hasNext()) {
			Entry<CompanyBasicInfo, CompNode> next = it.next();
			CompGraph componenta = new CompGraph();
			
			LinkedList<CompNode> toexpand = new LinkedList<>();
			toexpand.addLast(next.getValue());
			
			while(toexpand.size() != 0) {
				CompNode exp = toexpand.removeFirst();
				
				CompanyBasicInfo basic = new CompanyBasicInfo(exp.info.getNumeCompanie());
				int visited = lista.get(basic).getVisited();
				if (visited == 0) {
					if (exp.neighbours.size() != 0) {
						Iterator<CompNode> it2 = exp.neighbours.iterator();
						
						while(it2.hasNext()) {
							CompNode vec = it2.next();
							componenta.addNode(vec.info);
							toexpand.addLast(vec);
						}
					
					} else {
						componenta.addNode(exp.info);
					}
					
					lista.get(basic).setVisited(1);
				}
			}
			// add the component to the neo4j Database
			addCompNeoComp(graph, graphDb, "graph"+i);
			Runtime.getRuntime().gc();
			++i;
		}
		
	}
	
	
	/**
	 * Add Component to neo4j company graph database
	 * @param graph - the company graph component to be added
	 * @param graphdb - the graph database service
	 * @param labelName - the label of the connected component
	 */
	public static void addCompNeoComp(CompGraph graph, GraphDatabaseService graphdb,
			String labelName) {
		Iterator<Entry<CompanyBasicInfo, CompNode>> it = graph.getGraph().entrySet().iterator();
		HashMap<String, org.neo4j.graphdb.Node> created = new HashMap<>();
		
		// for each node get all the connections and add a link between them
		while(it.hasNext()) {
			Entry<CompanyBasicInfo, CompNode>  entr = it.next();
			CompNode nod = entr.getValue();
			
			ArrayList<String> names1 = new ArrayList<>();
			names1.addAll(nod.admins);
			names1.addAll(nod.fondatori);
			
			try ( Transaction tx = graphdb.beginTx() ) {
				// create the node for neo4j
				Label label = DynamicLabel.label(labelName);
				
				org.neo4j.graphdb.Node compNode;
				String numeComp = nod.info.getNumeCompanie();
				if (created.containsKey(numeComp)) {
					compNode = created.get(numeComp);
				} else {
					compNode = createNodeComp(numeComp, nod.info,
							graphdb, label);
					
					// save the reference to the already created node so that more
					// nodes won't be added
					created.put(numeComp, compNode);
				}
				// get all colaboration for the curent company
				ArrayList<CompNode> colab = nod.neighbours;
				Iterator<CompNode> itColab = colab.iterator();
				
				while(itColab.hasNext()) {
					CompNode colabNode = itColab.next();
					
					org.neo4j.graphdb.Node company;
					String compName = colabNode.info.getNumeCompanie();
					if (created.containsKey(compName)) {
						company = created.get(compName);
					} else {
						company = createNodeComp(compName, colabNode.info, graphdb, label);
						// save the reference to the already created node so that more
						// nodes won't be added
						created.put(compName, company);
					}
					
					// add a relationship between the two nodes 
					ArrayList<String> names2 = new ArrayList<>();
					names2.addAll(colabNode.admins);
					names2.addAll(colabNode.fondatori);
					createRelation(compNode, company, names1, names2);
				}
				
				tx.success();
			}
			
		}
	}
	
	/**
	 * Create a node in neo4j for a Company
	 * @param id - the id of the node / the name of the company
	 * @param graphdb - the neo4j database service
	 * @param label - the label of the node / the id of the connected component
	 * @return - the neo4j node
	 */
	public static org.neo4j.graphdb.Node createNodeComp(String id, CompanyInfo info,
			GraphDatabaseService graphdb, Label label) {
		org.neo4j.graphdb.Node compNode = graphdb.createNode(label);
		compNode.setProperty("id", id);
		compNode.setProperty(Constants.ACTIVITATE, info.getActivitatePrincipala());
		compNode.setProperty(Constants.ADMINS, info.getAdmins());
		compNode.setProperty(Constants.CAPITAL, info.getCapitalSocial());
		compNode.setProperty(Constants.CODUI, info.getCui());
		compNode.setProperty(Constants.DENUMIRE, info.getNumeCompanie());
		compNode.setProperty(Constants.DOMENIUACTIV, info.getDomeniuActivitate());
		compNode.setProperty(Constants.FONDATORI, info.getFondatori());
		compNode.setProperty(Constants.NRORDINE, info.getNrOrd());
		compNode.setProperty(Constants.SEDIUSOCIAL, info.getSediuSocial());
		
		return compNode;
	}
	
	private static enum RelTypes implements RelationshipType
	{
	    CONNECTED_NAME
	}
	
	
	public static Relationship createRelation(org.neo4j.graphdb.Node compNode1,
			org.neo4j.graphdb.Node compNode2, ArrayList<String> names1,
			ArrayList<String> names2) {
		Relationship relationship;
		relationship = compNode1.createRelationshipTo(compNode2, 
				RelTypes.CONNECTED_NAME);
		
		ArrayList<String> common = new ArrayList<>();
		for(int i = 0; i < names1.size(); i++) {
			for (int j = 0; j < names2.size(); j++) {
				String name1 = names1.get(i);
				String name2 = names2.get(j);
				if (name1.contains(name2) || name2.contains(name1)) {
					common.add(name2);
				}
			}
		}
		// the relationship between two company nodes is a list of all the 
		// names that have linked the two companies
		relationship.setProperty(Constants.BMAN, common.toString());
		
		return relationship;
	}
	
	
	
	/**
	 * Add a node to a existing business men graph, the information will be 
	 * extracted from a neo4j node
	 * @param graph
	 * @param node
	 */
	public static void addNode(CompGraph graph, org.neo4j.graphdb.Node node) {
		ArrayList<CompanyInfo> infos = buildCompanyInfo(node);
		for (int i = 0; i < infos.size(); i++) {
			graph.addNode(infos.get(i));
		}
	}
	
	public static CompanyInfo buildSingleInfo(org.neo4j.graphdb.Node node) {
		CompanyInfo info = new CompanyInfo();
		info.setNumeCompanie(node.getProperty(
				"id").toString());
		info.setSediuSocial(node.getProperty(
				Constants.SEDIUSOCIAL).toString());
		info.setNrOrd(node.getProperty(
				Constants.NRORDINE).toString());
		info.setFondatori(node.getProperty(
				Constants.FONDATORI).toString());
		info.setAdmins(node.getProperty(
				Constants.ADMINS).toString());
		info.setActivitatePrincipala(node.getProperty(
				Constants.ACTIVITATE).toString());
		info.setCui(node.getProperty(
				Constants.CODUI).toString());
		info.setDomeniuActivitate(node.getProperty(
				Constants.DOMENIUACTIV).toString());
		info.setCapitalSocial(node.getProperty(
				Constants.CAPITAL).toString());
		return info;
	}
	
	/**
	 * build company info from neo4j node in order to create a node in the
	 * business men graph 
	 * @param node - neo4j node resulted after a query to neo4j db
	 * @return	- companyInfo object
	 */
	public static ArrayList<CompanyInfo> buildCompanyInfo(org.neo4j.graphdb.Node node) {
		/// list of basic info about the admins and founders 
		ArrayList<CompanyInfo> informs = new ArrayList<>();
		// create the info to add a node to the graph
		informs.add(buildSingleInfo(node));
		
		
		Iterator<Relationship> it = node.getRelationships().iterator();
		while(it.hasNext()) {
			org.neo4j.graphdb.Node endNode = it.next().getEndNode();
			informs.add(buildSingleInfo(endNode));
		}
 		
		return informs;
	}
	
	/**
	 * Retun the connected component for a certain company name
	 * @param compName the name of the company
	 * @param graphdb the neo4j graph service
	 * @return - a company graph
	 */
	public static CompGraph getComponentByCompName(String compName, GraphDatabaseService graphdb) {
		CompGraph graph = new CompGraph();
		
		ExecutionEngine engine = new ExecutionEngine(graphdb);
		ExecutionResult result;
		
		try (Transaction ignored = graphdb.beginTx()) {
			String queryByName = CypherQueries.getLabelsByName(compName);
			result = engine.execute(queryByName);
			
			ArrayList<String> labels = new ArrayList<>();
			for (Map<String, Object> row : result) {
				for (Entry<String, Object> column : row.entrySet()) {
					String label = column.getValue().toString();
					label = label.substring(1, label.length() - 1);
					labels.add(label);
				}
			}
			
			System.out.println("Labels : " + labels);
			if (labels.size() >= 1) {
				String compLabel = labels.get(0);
				String queryByLabel = CypherQueries.getGraphByLabel(compLabel);
				System.out.println(queryByLabel);
				result = engine.execute(queryByLabel);
				
				Iterator<org.neo4j.graphdb.Node> n_column = result.columnAs( "n" );
				for (org.neo4j.graphdb.Node node : IteratorUtil.asIterable(n_column)) {
				    // TODO Add neo4j node into a compGraph
					addNode(graph, node);
					System.out.println(node + ": " + node.getProperty("id"));
				}
			} else {
				return null;
			}
		}
		return graph;
	}
	
	/**
	 * Get company graph by a filter, year, company location and activity
	 * @param location - the location of the company hq
	 * @param year - the year of the founding
	 * @param caen - the activity of the company
	 * @param graphdb - the neo4j database service
	 * @return - a company graph
	 */
	public static CompGraph getComponentByFilter(String location, String year, 
			String caen, GraphDatabaseService graphdb) {
		CompGraph compGraph = new CompGraph();
		
		ExecutionEngine engine = new ExecutionEngine(graphdb);
		ExecutionResult result;
		try (Transaction ignored = graphdb.beginTx()) {
			String queryByFilter = CypherQueries.filterComp(year, location, caen);
			if (queryByFilter != null) {
				result = engine.execute(queryByFilter);
			
				Iterator<org.neo4j.graphdb.Node> n_column = result.columnAs("n");
				for (org.neo4j.graphdb.Node node : IteratorUtil.asIterable(n_column)) {
				    addNode(compGraph, node);
				    System.out.println(node + ": " + node.getProperty("id"));
				    
				}
			} else {
				return null;
			}
		
		}
		
		return compGraph;
	}
	
}
