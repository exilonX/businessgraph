package company.graph;

import java.util.ArrayList;

import bman.info.CompanyInfo;

/**
 * Company Graph Node, contains information about the state of the node (visited
 * or not) about the company, all the Company Nodes connected with this node and
 * information about the founders and administrators of the company
 * @author merca
 *
 */
public class CompNode {
	private int visited;
	public CompanyInfo info;
	public ArrayList<CompNode> neighbours;
	public ArrayList<String> fondatori;
	public ArrayList<String> admins;
	
	public CompNode() {
		this.setVisited(0);
		this.info = new CompanyInfo();
		this.neighbours = new ArrayList<>();
		this.fondatori = new ArrayList<>();
		this.admins = new ArrayList<>();
	}
	
	public CompNode(int visited, CompanyInfo info) {
		this.setVisited(visited);
		this.info = info;
		this.neighbours = new ArrayList<>();
		this.fondatori = new ArrayList<>();
		this.admins = new ArrayList<>();
	}

	public int getVisited() {
		return visited;
	}

	public void setVisited(int visited) {
		this.visited = visited;
	}
}
