package bman.graph;

import java.util.HashMap;

/**
 * Filter a graph by certain criteria
 * @author merca
 *
 */
public class GraphFilter {

	public enum FilterFields {
		byBmanName,	// by the name of a business Man
		byCompName,	// by the name of the company
		byHQ,		// by the location of the headQuarters
		byActDom,	// by the domain of the activity
		byMainAct	// by the main activity of the Company
	}
	
	private Graph graph;
	
	public GraphFilter(Graph graph) {
		this.graph = graph;
	}
	
	public Graph filterGraph(HashMap<FilterFields, String> filterby) {
		Graph filtered = new Graph();
		
		return filtered;
	}

}
