package bman.graph;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.helpers.collection.IteratorUtil;

import company.graph.CompGraph;
import company.graph.CompNode;
import company.info.CompanyBasicInfo;


import bman.info.BasicInfo;
import bman.info.CompanyInfo;
import graphex.Constants;
import graphex.CypherQueries;


public class Neo4jIOBMan {
	
	/**
	 * Export businessMan graph into a Neo4j DB
	 * @param graph - the businessmen graph to be exported
	 * @param graphdb - the neo4j database Service
	 */
	public static void exportBManGraph(Graph graph, GraphDatabaseService graphdb) {
		HashMap<BasicInfo, Node> lista = graph.getGraph();
		Iterator<Map.Entry<BasicInfo, Node>> it = lista.entrySet().iterator();

		int i = 0;
		
		// iterate through all the nodes in the graph
		while(it.hasNext()) {
			System.out.println("Compute connected component " + i);
			
			Entry<BasicInfo, Node> next = it.next();
			// create a connected component
			Graph componenta = new Graph();
			
			// add all neighbour nodes to this list in order to expand 
			// and find all the nodes in the componnet (BFS)
			LinkedList<Node> toexpand = new LinkedList<>();
			toexpand.addLast(next.getValue());
			
			int j = 0;
			
			while(toexpand.size() != 0) {
				Node exp = toexpand.removeFirst();
				
				++j;
				BasicInfo basic = new BasicInfo(exp.getbManName());
				// check if the node was already visited and added to the 
				// component if so don't visit it again
				int visited = lista.get(basic).getVisited();
				if (visited == 0) {
					// check if the node has any links with other nodes, if not
					// just add the node to the neo4j database
					if (exp.getColaborations().size() != 0) {
						Iterator<Entry<Node, CompanyInfo>> it2 = exp.getColaborations().entrySet().iterator();
						while(it2.hasNext()) {
							Entry<Node, CompanyInfo> vec = it2.next();
							componenta.addNode(vec.getValue());
							toexpand.addLast(vec.getKey());
						}
						
					} else {
						componenta.addNode(exp.getOwned().get(0));
					}
					// mark the node as visited
					lista.get(basic).setVisited(1);
				}
			}
			
			String label = "comp" + i;
			addCompNeoBman(componenta, graphdb, label);
			
			++i;
			// force garbage collection
			Runtime.getRuntime().gc();
		}
	}
	
	
	private static enum RelTypes implements RelationshipType
	{
	    CONNECTED_COMPANY
	}
	
	/**
	 * Add a graph in Neo4j with a different label
	 * @param graph
	 * @param graphdb
	 * @param labelName
	 */
	public static void addCompNeoBman(Graph graph, GraphDatabaseService graphdb,
			String labelName) {
		Iterator<Entry<BasicInfo, Node>> it = graph.getGraph().entrySet().iterator();
		HashMap<String, org.neo4j.graphdb.Node> created = new HashMap<>();
		
		// for each node get all the connections and add a link between them
		while(it.hasNext()) {
			
			Entry<BasicInfo, Node>  entr = it.next();
			Node nod = entr.getValue();
			
			try ( Transaction tx = graphdb.beginTx() ) {
				// create the node for neo4j
				Label label = DynamicLabel.label(labelName);
				
				org.neo4j.graphdb.Node bManNode;
				if (created.containsKey(nod.getbManName())) {
					bManNode = created.get(nod.getbManName());
				} else {
					bManNode = createNodeBMan(nod.getbManName(),
							graphdb, label);
					// save the reference to the already created node so that more
					// nodes won't be added
					created.put(nod.getbManName(), bManNode);
				}
				HashMap<Node, CompanyInfo> colab = nod.getColaborations();
				
				Iterator<Entry<Node, CompanyInfo>> itColab = colab.entrySet().iterator();
				
				while(itColab.hasNext()) {
					Entry<Node, CompanyInfo> nodeInfo = itColab.next();
					org.neo4j.graphdb.Node bMan2;
					String bName = nodeInfo.getKey().getbManName();
					if (created.containsKey(bName)) {
						bMan2 = created.get(bName);
					} else {
						bMan2 = createNodeBMan(bName, graphdb, label);
						// save the reference to the already created node so that more
						// nodes won't be added
						created.put(bName, bMan2);
					}
					
					Relationship rel = createRelation(bManNode, bMan2, nodeInfo.getValue());
				}
				
				tx.success();
			}
			
		}
	}
	
	
	
	/**
	 * Create a node in neo4j for a BMan
	 * @param id
	 * @param graphdb
	 * @param label
	 * @return
	 */
	public static org.neo4j.graphdb.Node createNodeBMan(String id, 
			GraphDatabaseService graphdb, Label label) {
		org.neo4j.graphdb.Node bManNode = graphdb.createNode(label);
		bManNode.setProperty("id", id);
		
		return bManNode;
	}
	
	
	/**
	 * Add a node to a existing business men graph, the information will be 
	 * extracted from a neo4j node
	 * @param graph
	 * @param node
	 */
	public static void addNode(Graph graph, org.neo4j.graphdb.Node node) {
		ArrayList<CompanyInfo> infos = buildCompanyInfo(node);
		for (int i = 0; i < infos.size(); i++) {
			graph.addNode(infos.get(i));
		}
	}
	
	
	/**
	 * Make a relation between 2 nodes neo4j
	 * @param bManNode
	 * @param bMan2
	 * @param info
	 * @return
	 */
	public static Relationship createRelation(org.neo4j.graphdb.Node bManNode,
			org.neo4j.graphdb.Node bMan2, CompanyInfo info) {
		Relationship relationship;
		relationship = bManNode.createRelationshipTo(bMan2, RelTypes.CONNECTED_COMPANY);
		relationship.setProperty("numeComp", info.getNumeCompanie());
		relationship.setProperty("sediu", info.getSediuSocial());
		relationship.setProperty("nrOrd", info.getNrOrd());
		relationship.setProperty("fondatori", info.getFondatori());
		relationship.setProperty("admins", info.getAdmins());
		relationship.setProperty("activitate", info.getActivitatePrincipala());
		relationship.setProperty("cui", info.getCui());
		relationship.setProperty("domeniu", info.getDomeniuActivitate());
		relationship.setProperty("capital", info.getCapitalSocial());
		
		return relationship;
	}
	
	/**
	 * build company info from neo4j node in order to create a node in the
	 * business men graph 
	 * @param node - neo4j node resulted after a query to neo4j db
	 * @return	- companyInfo object
	 */
	public static ArrayList<CompanyInfo> buildCompanyInfo(org.neo4j.graphdb.Node node) {
		/// list of basic info about the admins and founders 
		ArrayList<CompanyInfo> informs = new ArrayList<>();
 		
 		
		Iterator<Relationship> it = node.getRelationships().iterator();
		while(it.hasNext()) {
			CompanyInfo info = new CompanyInfo();
			Relationship rel = it.next();
			info.setNumeCompanie(rel.getProperty(
					Constants.NUMECOMP).toString());
			info.setSediuSocial(rel.getProperty(
					Constants.SEDIUSOCIAL).toString());
			info.setNrOrd(rel.getProperty(
					Constants.NRORDINE).toString());
			info.setFondatori(rel.getProperty(
					Constants.FONDATORI).toString());
			info.setAdmins(rel.getProperty(
					Constants.ADMINS).toString());
			info.setActivitatePrincipala(rel.getProperty(
					Constants.ACTIVITATE).toString());
			info.setCui(rel.getProperty(
					Constants.CODUI).toString());
			info.setDomeniuActivitate(rel.getProperty(
					Constants.DOMENIUACTIV).toString());
			info.setCapitalSocial(rel.getProperty(
					Constants.CAPITAL).toString());
			informs.add(info);
		}
 		
		return informs;
	}
	
	/**
	 * Get a graph of the connected component of the node with the 
	 * name specified
	 * @param name - the name of the business man you are looking for
	 * @param graphdb - the connection to the neo4j database
	 * @return - a Business Men Graph that contains the specified BMan
	 */
	public static Graph getComponentByBName(String name, GraphDatabaseService graphdb) {
		Graph graph = new Graph();
		
		ExecutionEngine engine = new ExecutionEngine(graphdb);
		ExecutionResult result;
		
		try (Transaction ignored = graphdb.beginTx()) {
			String queryByName = CypherQueries.getLabelsByName(name);
			result = engine.execute(queryByName);
			
			ArrayList<String> labels = new ArrayList<>();
			for (Map<String, Object> row : result) {
				for (Entry<String, Object> column : row.entrySet()) {
					String label = column.getValue().toString();
					label = label.substring(1, label.length() - 1);
					labels.add(label);
				}
			}
			boolean ok = false;
			System.out.println("Labels : " + labels);
			if (labels.size() >= 1) {
				String compLabel = labels.get(0);
				String queryByLabel = CypherQueries.getGraphByLabel(compLabel);
				
				result = engine.execute(queryByLabel);
				
				Iterator<org.neo4j.graphdb.Node> n_column = result.columnAs( "n" );
				for (org.neo4j.graphdb.Node node : IteratorUtil.asIterable(n_column)) {
				    addNode(graph, node);
				    System.out.println(node + ": " + node.getProperty("id"));
				}
			} else {
				return null;
			}
		}
		
		return graph;
	}
	
	
	public static Graph getComponentByFilter(String location, String year, 
			String caen, GraphDatabaseService graphdb) {
		Graph bManGraph = new Graph();
		
		ExecutionEngine engine = new ExecutionEngine(graphdb);
		ExecutionResult result;
		try (Transaction ignored = graphdb.beginTx()) {
			String queryByName = CypherQueries.filter(year, location, caen);
			if (queryByName != null) {
				result = engine.execute(queryByName);
			
				
				Iterator<org.neo4j.graphdb.Node> n_column = result.columnAs("n");
				for (org.neo4j.graphdb.Node node : IteratorUtil.asIterable(n_column)) {
				    addNode(bManGraph, node);
				    //System.out.println(node + ": " + node.getProperty("id"));
				}
			} else {
				return null;
			}
		
		}
		
		return bManGraph;
	}
	

}
