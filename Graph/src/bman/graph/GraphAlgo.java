package bman.graph;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import bman.info.BasicInfo;
import bman.info.CompanyInfo;

/**
 * Implementation of various graph algorithms
 * @author Merca Ionel
 *
 */
public class GraphAlgo {
	private Graph graph;

	
	public GraphAlgo() {
		this.graph = new Graph();
	}
	
	public GraphAlgo(Graph graf) {
		this.graph = graf;
	}
	
	/**
	 * Extract the connected components from a graph
	 * @return - list of all the connected components
	 */
	public ArrayList<Graph> extractComponents() {
		HashMap<BasicInfo, Node> lista = this.graph.getGraph();
		ArrayList<Graph> componente = new ArrayList<>();
		Iterator<Map.Entry<BasicInfo, Node>> it = lista.entrySet().iterator();

		while(it.hasNext()) {
			Entry<BasicInfo, Node> next = it.next();
			Graph componenta = new Graph();
			
			LinkedList<Node> toexpand = new LinkedList<>();
			toexpand.addLast(next.getValue());
			
			while(toexpand.size() != 0) {
				Node exp = toexpand.removeFirst();
				
				BasicInfo basic = new BasicInfo(exp.getbManName());
				int visited = lista.get(basic).getVisited();
				if (visited == 0) {
					// iterate through all the linked nodes
					Iterator<Entry<Node, CompanyInfo>> it2 = 
							exp.getColaborations().entrySet().iterator();
					
					while(it2.hasNext()) {
						Entry<Node, CompanyInfo> vec = it2.next();
						componenta.addNode(vec.getValue());
						toexpand.addLast(vec.getKey());
					}
					
					lista.get(basic).setVisited(1);
				}
			}
			
			componente.add(componenta);
		}
		
		return componente;
	}
	
	/**
	 * returns the graph with the maximum size
	 * @param comp
	 * @return
	 */
	public Graph getMaxGraph(ArrayList<Graph> comp) {
		int k = 0;
		int max_size = -1;
		Graph max_graf = null;
		
		for (int i = 0; i < comp.size(); i++) {
			int size = comp.get(i).getGraph().size();
			if (size > 2) {
				k++;
				if (size > max_size ) {
					max_size = size;
					max_graf = comp.get(i);
				}
			}
		}
		
		System.out.println("K : " + k);
		System.out.println(comp.size());
		
		return max_graf;
	}
	
	/**
	 * Comparator class used for sorting a list of graphs after size
	 * @author Merca Ionel
	 *
	 */
	public class GraphComparator implements java.util.Comparator<Graph> {
		
		@Override
		public int compare(Graph arg0, Graph arg1) {
			int size1 = arg0.getGraph().size();
			int size2 = arg1.getGraph().size();
			return size2 - size1;
		}
		
	}
	
	/**
	 * return the nth biggest graph from the comp arrayList
	 * @param n  - the number of returned graphs
	 * @param comp - the list of graph to select from
	 * @return  - a linked list of the resulting graph the size will be n or less
	 */
	public LinkedList<Graph> getNMaxGraph(int n, ArrayList<Graph> comp) {
		LinkedList<Graph> max = new LinkedList<>();
		
		System.out.println("Inainte: " + comp.get(0).getGraph().size());
		Collections.sort(comp, new GraphComparator());
		System.out.println("Dupa " + comp.get(0).getGraph().size());
		
		n = n > comp.size() ? comp.size() : n;
		
		for (int i = 0; i < n; i++) {
			max.add(comp.get(i));
		}
		
		return max;
	}
	
	/**
	 * Check if a graph is complete, if exists a edge between any two nodes
	 * @return
	 */
	public boolean checkComplete() {
		int n = graph.getGraph().size();
		Iterator<Entry<BasicInfo, Node>> it = this.graph.getGraph().entrySet().iterator();
		
		while(it.hasNext()) {
			Entry<BasicInfo, Node> entr = it.next();
			if (entr.getValue().getColaborations().size() != n) {
				return false;
			}
		}
		return true;
	}
	
	public void removeSingleNodes() {
		Iterator<Entry<BasicInfo, Node>> it = this.graph.getGraph().entrySet().iterator();
		
		while(it.hasNext()) {
			Entry<BasicInfo, Node> entr = it.next();
			if (entr.getValue().getColaborations().size() == 0) {
				it.remove();
			}
		}
		
	}
	
}
