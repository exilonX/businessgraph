package abstr.info;

import company.info.CompanyBasicInfo;

public abstract class AbstractBasicInfo {

	private String identifier;
	
	public AbstractBasicInfo() {
		this.identifier = new String();
	}
	
	public AbstractBasicInfo(String identifier) {
		this.identifier = identifier;
	}
	
	public String getIdentifier() {
		return this.identifier;
	}
	
	public void setIdentifier(String id) {
		this.identifier = id;
	}
	
	@Override
	public int hashCode() {
		return this.identifier.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return this.identifier.equals(((AbstractBasicInfo)obj).getIdentifier());
	}
	
	@Override
	public String toString() {
		return "Identifier Name : " + identifier + "\n" ;
	}

}
