package graphex;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.io.exporter.api.ExportController;

import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openide.util.Lookup;

import company.graph.CompGraph;
import company.graph.CompNode;
import company.info.CompanyBasicInfo;

import bman.graph.Graph;
import bman.graph.Node;
import bman.info.BasicInfo;
import bman.info.CompanyInfo;


public class Export {
	
	/**
	 * export a graph as JSON 
	 * @param filename
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static JSONObject exportJSON(String filename, Graph graph) {
		JSONObject main = new JSONObject();
		JSONArray nodes = new JSONArray();
		JSONArray edges = new JSONArray();
		
		Iterator<Entry<BasicInfo, Node>> it = graph.getGraph().entrySet().iterator();
		
		int id_edge = 0;
		while(it.hasNext()) {
			Entry<BasicInfo, Node> next = it.next();
			JSONObject node = new JSONObject();
			if (next.getValue().getColaborations().size() != 0) {
				node.put("id", next.getKey().getIdentifier());
				node.put("label", next.getKey().getIdentifier());
				node.put("size", next.getValue().getOwned().size() + next.getValue().getColaborations().size());
				
				nodes.add(node);
				
				Node nod = next.getValue();
				Iterator<Entry<Node, CompanyInfo>> itr = 
						nod.getColaborations().entrySet().iterator();
				while(itr.hasNext()) {
					id_edge++;
					Node n = itr.next().getKey();
					JSONObject edge = new JSONObject();
					edge.put("id", Integer.toString(id_edge));
					edge.put("source", next.getKey().getIdentifier());
					edge.put("target", n.getbManName());
					edges.add(edge);
				}
				
			}
		}
		
		main.put("nodes", nodes);
		main.put("edges", edges);
		
		if (filename != null) {
			Writer write = null;
			try {
				write = new PrintWriter(new File(filename));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			
			try {
				main.writeJSONString(write);
				write.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	
		return main;
	}
	
	/**
	 * export companies graph as JSON
	 * @param filename
	 * @param graf
	 */
	@SuppressWarnings("unchecked")
	public static JSONObject exportJSON(String filename, CompGraph graf) {
		JSONObject main = new JSONObject();
		JSONArray nodes = new JSONArray();
		JSONArray edges = new JSONArray();
		
		Iterator<Entry<CompanyBasicInfo, CompNode>> it = graf.getGraph().entrySet().iterator();
		
		int id_edge = 0;
		while(it.hasNext()) {
			Entry<CompanyBasicInfo, CompNode> next = it.next();
			JSONObject node = new JSONObject();
			
			if (next.getValue().neighbours.size() != 0) {
				if (!next.getKey().getIdentifier().equals("")) {
					node.put("id", next.getKey().getIdentifier());
					node.put("label", next.getKey().getIdentifier());
					node.put("size", next.getValue().neighbours.size());
					nodes.add(node);
					
					CompNode nod = next.getValue();
					Iterator<CompNode> itr = 
							nod.neighbours.iterator();
					while(itr.hasNext()) {
						id_edge++;
						CompNode n = itr.next();
						JSONObject edge = new JSONObject();
						if (!n.info.getNumeCompanie().equals("")) {
							edge.put("id", Integer.toString(id_edge));
							edge.put("source", next.getKey().getIdentifier());
							edge.put("target", n.info.getNumeCompanie());
							edges.add(edge);
						}
					}
				}
			}
		}
		
		main.put("nodes", nodes);
		main.put("edges", edges);
		
		if (filename != null) {
			Writer write = null;
			try {
				write = new PrintWriter(new File(filename));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			
			
			
			try {
				main.writeJSONString(write);
				write.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return main;
	}
	
	
	public static String correctDiact(String incorect) {
		Map<Character, Character> charReplacementMap = new HashMap<Character, Character>();
		charReplacementMap.put('Â', 'A');
		charReplacementMap.put('â', 'a');
		charReplacementMap.put('Ă', 'A');
		charReplacementMap.put('ă', 'a');
		charReplacementMap.put('î', 'i');
		charReplacementMap.put('Î', 'I');
		charReplacementMap.put('ș', 's');
		charReplacementMap.put('Ș', 'S');
		charReplacementMap.put('ț', 't');
		charReplacementMap.put('Ț', 'T');
		
		StringBuilder builder = new StringBuilder();

		for (char currentChar : incorect.toCharArray()) {
		    Character replacementChar = charReplacementMap.get(currentChar);
		    builder.append(replacementChar != null ? replacementChar : currentChar);
		}

		String newString = builder.toString();
		return newString;
	}
	
	/**
	 * Export a graph as a GEPHI graph
	 * @param graph
	 */
	public static GraphModel makeGephiGraph(Graph graph ) {
		//Init a project - and therefore a workspace
		ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
		pc.newProject();
        @SuppressWarnings("unused")
		Workspace workspace = pc.getCurrentWorkspace();
		
		//Get a graph model - it exists because we have a workspace
        GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
        DirectedGraph directedGraph = graphModel.getDirectedGraph();
        
        Iterator<Entry<BasicInfo, Node>> it = graph.getGraph().entrySet().iterator();
        
        // HashMap to check if a node was already created
        // name -> gephi node
        HashMap<String, org.gephi.graph.api.Node> nameNode = new HashMap<>();
        int i = 0;
        int j = 0;
		while(it.hasNext()) {
			System.out.println(i);
			Entry<BasicInfo, Node> next = it.next();
			i++;
			if (next.getValue().getColaborations().size() != 0) {
				j++;
				String identifier = correctDiact(next.getKey().getIdentifier()).intern();
				org.gephi.graph.api.Node n0 = nameNode.get(identifier);
				
				if (n0 == null) {
					// create a new node if the node was not created already 
					// when iterating through one of it's neightbours
					float size = next.getValue().getOwned().size() + 
							next.getValue().getColaborations().size();
					
					n0 = graphModel.factory().newNode(identifier);
			        n0.getNodeData().setLabel(identifier);
			        n0.getNodeData().setSize(size);
			        
			        // add the node to the already created list
			        nameNode.put(identifier, n0);
			        
			        // add the node to the graph
			        directedGraph.addNode(n0);
				}
				
				Node nod = next.getValue();
				Iterator<Entry<Node, CompanyInfo>> itr = 
						nod.getColaborations().entrySet().iterator();
				
				// iterate through all the neighbors and add edges
				while(itr.hasNext()) {
					
					Node n = itr.next().getKey();
					
					String name = correctDiact(n.getbManName()).intern();
					org.gephi.graph.api.Node n1 = nameNode.get(name);
					if (n1 == null) {
						float size = n.getOwned().size() + 
								n.getColaborations().size();
						
						n1 = graphModel.factory().newNode(name);
				        n1.getNodeData().setLabel(name);
				        n1.getNodeData().setSize(size);
				        nameNode.put(name, n1);
				        directedGraph.addNode(n1);
					}
					
					// add a edge between the two nodes
					Edge e1 = graphModel.factory().newEdge(n0, n1, 2f, true);
					directedGraph.addEdge(e1);
				}
				
			}
		}
		
		System.out.println(i + "  " + j);
		System.out.println(directedGraph.getNodeCount() + "   " + directedGraph.getEdgeCount());
		return graphModel;
	}
	
	/**
	 * Export a graph as a GEPHI graph
	 * @param graph
	 */
	public static GraphModel makeGephiGraph(CompGraph graph ) {
		//Init a project - and therefore a workspace
		ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
		pc.newProject();
        @SuppressWarnings("unused")
		Workspace workspace = pc.getCurrentWorkspace();
		
		//Get a graph model - it exists because we have a workspace
        GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
        DirectedGraph directedGraph = graphModel.getDirectedGraph();
        
        Iterator<Entry<CompanyBasicInfo, CompNode>> it = graph.getGraph().entrySet().iterator();
        
        // HashMap to check if a node was already created
        // company Name -> gephi node
        HashMap<String, org.gephi.graph.api.Node> nameNode = new HashMap<>();
        int i = 0;
        int j = 0;
		while(it.hasNext()) {
			Entry<CompanyBasicInfo, CompNode> next = it.next();
			i++;
			System.out.println(i);
			if (next.getValue().neighbours.size() != 0) {
				j++;
				String identifier = correctDiact(next.getKey().getIdentifier()).intern();
				org.gephi.graph.api.Node n0 = nameNode.get(identifier);
				
				if (n0 == null) {
					// create a new node if the node was not created already 
					// when iterating through one of it's neightbours
					float size = next.getValue().neighbours.size();
					
					n0 = graphModel.factory().newNode(identifier);
			        n0.getNodeData().setLabel(identifier);
			        n0.getNodeData().setSize(size);
			        
			        // add the node to the already created list
			        nameNode.put(identifier, n0);
			        
			        // add the node to the graph
			        directedGraph.addNode(n0);
				}
				
				CompNode nod = next.getValue();
				Iterator<CompNode> itr = 
						nod.neighbours.iterator();
				
				// iterate through all the neighbors and add edges
				while(itr.hasNext()) {
					CompNode n = itr.next();
					
					String name = correctDiact(n.info.getNumeCompanie()).intern();
					org.gephi.graph.api.Node n1 = nameNode.get(name);
					if (n1 == null) {
						float size = n.neighbours.size();
						
						n1 = graphModel.factory().newNode(name);
				        n1.getNodeData().setLabel(name);
				        n1.getNodeData().setSize(size);
				        nameNode.put(name, n1);
				        directedGraph.addNode(n1);
					}
					
					// add a edge between the two nodes
					Edge e1 = graphModel.factory().newEdge(n0, n1, 2f, true);
					directedGraph.addEdge(e1);
				}
				
			}
		}
		
		System.out.println(i + "  " + j);
		System.out.println("Node Count " + directedGraph.getNodeCount() +
				"Edge Count " + "   " + directedGraph.getEdgeCount());
		return graphModel;
	}
	
	public static void exportGEXF(String outputFile, GraphModel model) {
		File out = new File(outputFile);
		ExportController ec = Lookup.getDefault().
                lookup( ExportController.class );
        
		System.out.println( out.getAbsoluteFile() );
        
        try {
			ec.exportFile(out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
