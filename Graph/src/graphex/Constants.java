package graphex;

/**
 * Class that contains constants
 * @author merca
 *
 */
public class Constants {
	/**
	 * Constants that are used to get data out of MongoDB (columns in the db)
	 */
	public static String SediuSocial = "sediu_social";
	public static String ActivitatePrincipala = "activ_princ";
	public static String DurataFunct =  "durata_func";
	public static String Admin = "admin";
	public static String Denumire = "denumire";
	public static String Fondatori = "fondatori";
	public static String NrOrdine = "nr_ordine";
	public static String Capital = "capital";
	public static String DomActivitate = "dom_activ";
	public static String CUI = "cui";
	public static String statusAdmin = "admin";
	public static String statusFound = "fondator";
	public static String statusFA = "admin/fondator";
	
	public static String[] listOfNotNames = {"RON", "ROL", "DEPLINE",
				"USD", "EUR", "EXERCITATE", "SEPARAT", "EURO",
				"CONFORM", "ACT", "CONSTITUTIV", "CAPITAL", "PIB",
				"HRB", "REGISTRUL", "COMERȚULUI", "JUDECĂTORIEI", "ESSEN",
				"XXIV", "Nr.", "PUBLICAȚII", "ALE", "AGENȚILOR", "ECONOMICI",
				"NELIMITATE", "DOLAR", "ADMINISTRATOR", "REPREZENTANT", "SPCLEP",
				"CAEN", "CNP", "SPCJEP", "București"
				};
	
	/** constants for neo4j Properties **/
	public static String SEDIUSOCIAL = "sediu";
	public static String ACTIVITATE = "activitate";
	public static String ADMINS = "admins";
	public static String DENUMIRE = "id";
	public static String FONDATORI = "fondatori";
	public static String NRORDINE = "nrOrd";
	public static String CAPITAL = "capital";
	public static String DOMENIUACTIV = "domeniu";
	public static String CODUI = "cui";
	public static String NUMECOMP = "numeComp";
	public static String BMAN = "bman";
	
}
