package graphex;

public class CypherQueries {

	/** 
	 * Return the query to get all the labels for a node that has the id
	 * similar to the nameId specified
	 * @param nameId - the name id of the node
	 * @return - the Cypher query
	 */
	public static String getLabelsByName(String nameId) {
//		String queryByName = "MATCH (n) WHERE n.id =~ '.*" + nameId + 
//				".*' RETURN labels(n)";
		String queryByName = "Start n=node(*) " + 
			    "match (n)-->() with n,count(*) as rel_cnt " + 
				"where rel_cnt >= 2 and n.id =~ '(?i).*" + nameId + ".*' " +
				"return labels(n) order by rel_cnt DESC limit 1000";
		return queryByName;
	}
	
	/**
	 * Return the query to get all the nodes that have the label specified
	 * @param label - the label of the nodes 
	 * @return - the query 
	 */
	public static String getGraphByLabel(String label) {
		String queryByLabel = "match (n: " + label + " ) return n";
		return queryByLabel;
	}
	
	
	/**
	 * Cypher query that filters the nodes of the graph, based on the input year,
	 * location and CAEN number the graph will contain nodes that have the 
	 * requested information
	 * @param year 
	 * @param location 
	 * @param CAEN
	 * @return
	 */
	public static String filter(String year, String location , String CAEN) {
		StringBuilder filterQuery = new StringBuilder();
		filterQuery.append("MATCH (n)-[r]-() WHERE ");
		int ok = 0;
		if (year != null) {
			filterQuery.append("r.nrOrd =~ '.*" + year.trim() + ".*' AND ");
			ok = 1;
		}
		
		if (location  != null) {
			filterQuery.append("r.sediu =~ '.*" + location .trim() + ".*' AND ");
			ok = 1;
		}
		
		if (CAEN != null) {
			filterQuery.append("r.activitate =~ '.*" + CAEN.trim() + ".*' AND ");
			ok = 1;
		}
		
		if (ok == 1) {
			filterQuery.delete(filterQuery.length() - 4, filterQuery.length());
			filterQuery.append("return distinct n");
		} else {
			System.err.println("At least one parameter shoud be not null");
			return null;
		}
		
		System.out.println(filterQuery.toString());
		return filterQuery.toString();
	}
	
	/**
	 * Cypher query that filters the nodes of the graph, based on the input year,
	 * location and CAEN number the graph will contain nodes that have the 
	 * requested information
	 * @param year 
	 * @param location 
	 * @param CAEN
	 * @return
	 */
	public static String filterComp(String year, String location , String CAEN) {
		StringBuilder filterQuery = new StringBuilder();
		filterQuery.append("MATCH (n) WHERE ");
		int ok = 0;
		if (year != null) {
			filterQuery.append("n.nrOrd =~ '.*" + year.trim() + ".*' AND ");
			ok = 1;
		}
		
		if (location  != null) {
			filterQuery.append("n.sediu =~ '.*" + location .trim() + ".*' AND ");
			ok = 1;
		}
		
		if (CAEN != null) {
			filterQuery.append("n.domeniu =~ '.*" + CAEN.trim() + ".*' AND ");
			ok = 1;
		}
		
		if (ok == 1) {
			filterQuery.delete(filterQuery.length() - 4, filterQuery.length());
			filterQuery.append("return distinct n");
		} else {
			System.err.println("At least one parameter shoud be not null");
			return null;
		}
		
		System.out.println(filterQuery.toString());
		return filterQuery.toString();
	}

}
