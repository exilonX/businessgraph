import urllib, os
import sys

url = "http://s3-eu-west-1.amazonaws.com/mgax-mof/"

i = 0 
year = "mof4_" + sys.argv[1] + "_"
path_mof = "mofDown/" + sys.argv[1] + "/"

if not os.path.exists(path_mof):
    os.makedirs(path_mof)

while (True):
	i += 1
	k = 4 - len(str(i))
	end = k * '0' + str(i) + ".pdf"
	newurl = url + year + end
	path = path_mof + year + end
	print newurl
	print path
	u = urllib.urlretrieve(newurl, path)
	print u[1]
	if i > 7500:
		break
	if not "Etag" in u[1] :
		os.remove(path)
